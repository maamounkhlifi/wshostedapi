﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Filters;
using Entities;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Entities.Extensions;

namespace ActionFilters
{
    public class ValidateEntityExists<T> : IActionFilter where T : class, IEntity
    {
        private readonly WSHostedAPIContext _context;

        public ValidateEntityExists(WSHostedAPIContext context)
        {
            _context = context;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            int id;

            if (context.ActionArguments.ContainsKey("Id"))
            {
                id = (int)context.ActionArguments["Id"];
            }
            else
            {
                context.Result = new BadRequestObjectResult("Bad id parameter");
                return;
            }

            var exists = _context.Set<T>().Any(x => x.Id.Equals(id));
            if (!exists)
            {
                context.Result = new NotFoundResult();
            }            
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }
    }
}
