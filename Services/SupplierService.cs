﻿using Contracts;
using Contracts.Services;
using Entities.Models;
using System;

namespace Services
{
    public class SupplierService : ServiceBase<Supplier>, ISupplierService
    {
        private readonly ISupplierRepository repository;
        private readonly IRepositoryWrapper repositoryWrapper;

        public SupplierService(ISupplierRepository repository, IRepositoryWrapper repositoryWrapper)
            : base(repository, repositoryWrapper)
        {
        }


    }
}
