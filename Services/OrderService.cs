﻿using Contracts;
using Contracts.Services;
using Entities.Models;
using System;

namespace Services
{
    public class OrderService : ServiceBase<Order>, IOrderService
    {
        private readonly IOrderRepository repository;
        private readonly IRepositoryWrapper repositoryWrapper;

        public OrderService(IOrderRepository repository, IRepositoryWrapper repositoryWrapper)
            : base(repository, repositoryWrapper)
        {
        }


    }
}
