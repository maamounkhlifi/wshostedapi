﻿using Contracts;
using Contracts.Services;
using Entities.Models;
using System;

namespace Services
{
    public class OrderItemService : ServiceBase<OrderItem>, IOrderItemService
    {
        private readonly IOrderItemRepository repository;
        private readonly IRepositoryWrapper repositoryWrapper;

        public OrderItemService(IOrderItemRepository repository, IRepositoryWrapper repositoryWrapper)
            : base(repository, repositoryWrapper)
        {
        }


    }
}
