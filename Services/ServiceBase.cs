﻿using Contracts;
using Contracts.Services;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ServiceBase<T> : IServiceBase<T>
    {
        internal readonly IRepositoryBase<T> _repository;
        internal readonly IRepositoryWrapper _repositoryWrapper;
        public ServiceBase(IRepositoryBase<T> repository, IRepositoryWrapper repositoryWrapper)
        {
            _repository = repository;
            _repositoryWrapper = repositoryWrapper;
        }

        public void Create(T entity)
        {
            _repository.Create(entity);
        }

        public void Delete(T entity)
        {
            _repository.Delete(entity);
        }

        public System.Linq.IQueryable<T> FindAll()
        {
            return _repository.FindAll();
        }

        public Task<IEnumerable<T>> FindAllAsync()
        {
            return _repository.FindAllAsync();
        }

        public System.Linq.IQueryable<T> FindByCondition(System.Linq.Expressions.Expression<Func<T, bool>> expression)
        {
            return _repository.FindByCondition(expression);
        }

        public Task<IEnumerable<T>> FindByConditionAsync(System.Linq.Expressions.Expression<Func<T, bool>> expression)
        {
            return _repository.FindByConditionAsync(expression);
        }

        public Task<IEnumerable<T>> FindByConditionAsync(System.Linq.Expressions.Expression<Func<T, bool>> expression, params System.Linq.Expressions.Expression<Func<T, object>>[] includeExpressions)
        {
            return _repository.FindByConditionAsync(expression, includeExpressions);
        }

        public void Update(T entity)
        {
            _repository.Update(entity);
        }

        public async Task Save()
        {
            await _repositoryWrapper.Save();
        }

        public Task<GridResult<T>> FindGridDataAsync(GridFilter<T> gridFilters)
        {
            return _repository.FindGridDataAsync(gridFilters);
        }

        public List<string> GetAllRecordsForField(string query, string field)
        {
            return _repository.GetAllRecordsForField(query, field);
        }
    }
}
