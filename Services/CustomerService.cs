﻿using Contracts;
using Contracts.Services;
using Entities.Models;
using System;

namespace Services
{
    public class CustomerService : ServiceBase<Customer>, ICustomerService
    {
        private readonly ICustomerRepository repository;
        private readonly IRepositoryWrapper repositoryWrapper;

        public CustomerService(ICustomerRepository repository, IRepositoryWrapper repositoryWrapper)
            : base(repository, repositoryWrapper)
        {
        }


    }
}
