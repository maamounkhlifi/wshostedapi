﻿using Contracts;
using Contracts.Services;
using Entities.Models;
using System;

namespace Services
{
    public class ProductService : ServiceBase<Product>, IProductService
    {
        private readonly IProductRepository repository;
        private readonly IRepositoryWrapper repositoryWrapper;

        public ProductService(IProductRepository repository, IRepositoryWrapper repositoryWrapper)
            : base(repository, repositoryWrapper)
        {
        }


    }
}
