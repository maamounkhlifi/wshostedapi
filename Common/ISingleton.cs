﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public interface ISingleton
    {
        string ConnectionString();
        string JwtSecret();
        Auth0Credentials Auth0Options();
    }
}
