﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public class Auth0Credentials
    {
        public string Domain { get; set; }
        public string ClientID { get; set; }
        public string RedirectUri { get; set; }
        public string Authority { get; set; }
        public string Audience { get; set; }
    }
}
