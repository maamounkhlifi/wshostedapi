﻿using Microsoft.Extensions.Configuration;
using System;

namespace Common
{
    public class Singleton: ISingleton
    {        
        public Singleton(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
            _jwtSecret = configuration["JWT:Secret"];
            _auth0Credentials = new Auth0Credentials();
            _auth0Credentials.Audience = configuration["Auth0:Audience"];
            _auth0Credentials.Authority = configuration["Auth0:Authority"];
            _auth0Credentials.ClientID = configuration["Auth0:ClientID"];
            _auth0Credentials.Domain = configuration["Auth0:Domain"];
            _auth0Credentials.RedirectUri = configuration["Auth0:RedirectUri"];            
        }

        private string _connectionString;

        public string ConnectionString()
        {
            return _connectionString;
        }

        private string _jwtSecret;

        public string JwtSecret()
        {
            return _jwtSecret;
        }

        private Auth0Credentials _auth0Credentials;

        public Auth0Credentials Auth0Options()
        {
            return _auth0Credentials;
        }
    }
}
