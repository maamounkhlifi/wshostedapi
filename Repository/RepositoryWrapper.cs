﻿using Contracts;
using Entities.Models;
using System.Threading.Tasks;

namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private WSHostedAPIContext _wshostedapiContext;
        private ICustomerRepository _customer;
        private IOrderRepository _order;
        private IOrderItemRepository _orderItem;
        private IProductRepository _product;
        private ISupplierRepository _supplier;
        private IUserRepository _user;

        public ICustomerRepository Customer
        {
            get
            {
                if (_customer == null)
                {
                    _customer = new CustomerRepository(_wshostedapiContext);
                }

                return _customer;
            }
        }

        public IOrderRepository Order
        {
            get
            {
                if (_order == null)
                {
                    _order = new OrderRepository(_wshostedapiContext);
                }

                return _order;
            }
        }

        public IOrderItemRepository OrderItem
        {
            get
            {
                if (_orderItem == null)
                {
                    _orderItem = new OrderItemRepository(_wshostedapiContext);
                }

                return _orderItem;
            }
        }

        public IProductRepository Product
        {
            get
            {
                if (_product == null)
                {
                    _product = new ProductRepository(_wshostedapiContext);
                }

                return _product;
            }
        }

        public ISupplierRepository Supplier
        {
            get
            {
                if (_supplier == null)
                {
                    _supplier = new SupplierRepository(_wshostedapiContext);
                }

                return _supplier;
            }
        }


            public IUserRepository User
        {
            get
            {
                if (_user == null)
                {
                    _user = new UserRepository(_wshostedapiContext);
                }

                return _user;
            }
        }

        public RepositoryWrapper(WSHostedAPIContext wshostedapiContext)
        {
            _wshostedapiContext = wshostedapiContext;
        }

        public async Task Save()
        {
            await _wshostedapiContext.SaveChangesAsync();
        }
    }
}