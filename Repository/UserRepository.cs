﻿using Contracts;
using Entities.Models;

namespace Repository
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(WSHostedAPIContext wshostedAPIContext)
            : base(wshostedAPIContext)
        {
        }
    }
}