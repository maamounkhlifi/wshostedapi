﻿using Contracts;
using Entities.Models;

namespace Repository
{
    public class OrderItemRepository : RepositoryBase<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(WSHostedAPIContext wshostedAPIContext)
            : base(wshostedAPIContext)
        {
        }
    }
}