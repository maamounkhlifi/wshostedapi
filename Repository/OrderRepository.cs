﻿using Contracts;
using Entities.Models;

namespace Repository
{
    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        public OrderRepository(WSHostedAPIContext wshostedAPIContext)
            : base(wshostedAPIContext)
        {
        }
    }
}