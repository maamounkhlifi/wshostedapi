﻿using Contracts;
using Entities.Models;

namespace Repository
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(WSHostedAPIContext wshostedAPIContext)
            : base(wshostedAPIContext)
        {
        }
    }
}