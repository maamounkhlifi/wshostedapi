﻿using Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using Entities.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using LinqKit;
using Helpers.Extensions;

namespace Repository
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected WSHostedAPIContext WSHostedAPIContext { get; set; }

        public RepositoryBase(WSHostedAPIContext WSHostedAPIContext)
        {
            this.WSHostedAPIContext = WSHostedAPIContext;
        }

        public IQueryable<T> FindAll()
        {
            return this.WSHostedAPIContext.Set<T>().AsNoTracking();
        }

        public async Task<IEnumerable<T>> FindAllAsync()
        {
            return await this.WSHostedAPIContext.Set<T>().AsNoTracking().ToListAsync<T>();
        }

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return this.WSHostedAPIContext.Set<T>().AsNoTracking().Where(expression).AsNoTracking();
        }

        public async Task<IEnumerable<T>> FindByConditionAsync(Expression<Func<T, bool>> expression)
        {
            return await this.WSHostedAPIContext.Set<T>().AsNoTracking().Where(expression).ToListAsync();
        }

        public async Task<IEnumerable<T>> FindByConditionAsync(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] includeExpressions)
        {
            IQueryable<T> dbSet = this.WSHostedAPIContext.Set<T>();
            foreach (Expression<Func<T, object>> includeExpression in includeExpressions)
            {
                dbSet = dbSet.Include(includeExpression);
            }
            return await dbSet.AsNoTracking().Where(expression).ToListAsync();
        }

        public void Create(T entity)
        {
            this.WSHostedAPIContext.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            this.WSHostedAPIContext.Set<T>().Update(entity);
        }

        public void Delete(T entity)
        {
            this.WSHostedAPIContext.Set<T>().Remove(entity);
        }
        public async Task<int> Count()
        {
            return await this.WSHostedAPIContext.Set<T>().CountAsync();
        }

        public async Task<GridResult<T>> FindGridDataAsync(GridFilter<T> gridFilters)
        {
            IQueryable<T> dataQuery = this.WSHostedAPIContext.Set<T>().AsExpandable().WhereDataHas<T>(gridFilters.filter); // AsExpandable required by LinqKit
            return new GridResult<T>
            {
                Data = dataQuery.OrderBy<T>(gridFilters.SortField, gridFilters.SortOrder)
                                .Skip(gridFilters.PageSize * gridFilters.PageNumber)
                                .Take(gridFilters.PageSize),
                TotalRecords = await dataQuery.CountAsync<T>()
            };
        }

        public List<string> GetAllRecordsForField(string query, string field)
        {
            field = field[0].ToString().ToUpper() + field.Substring(1);
            return this.WSHostedAPIContext.Set<T>().GetAllRecordsForField<T>(query, field);

        }

    }

}
