﻿using Common.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    public class User : IEntity
    {
        /// <summary>
        /// The id of the user
        /// </summary>
        /// <example></example>                
        public int? Id { get; set; }

        /// <summary>
        /// The first name of the user
        /// </summary>
        /// <example>Maamoun</example>
        [Required(ErrorMessage = "First Name is required")]
        [StringLength(40)]
        public string FirstName { get; set; }

        /// <summary>
        /// The last name of the user
        /// </summary>
        /// <example>Khlifi</example>
        [Required(ErrorMessage = "Last Name is required")]
        [StringLength(40)]
        public string LastName { get; set; }

        /// <summary>
        /// The username of the user
        /// </summary>
        /// <example>mkhlifi</example>
        [Required(ErrorMessage = "Username is required")]
        [StringLength(40)]
        public string Username { get; set; }

        /// <summary>
        /// The password of the user
        /// </summary>
        /// <example>password!*</example>
        [Required(ErrorMessage = "Password is required")]
        [StringLength(40)]
        [NotMapped]
        public string Password { get; set; }

        /// <summary>
        /// JWT Token
        /// </summary>
        /// <example>eze4e9sf64eEfFdG8q6df5SeDz5dfF6f5sdSfDzFeZfd7s8fFSzeDf5ds4Z7dzE5e</example>
        [SwaggerExclude]
        [JsonIgnore]
        public byte[] PasswordHash { get; set; }

        [SwaggerExclude]
        [JsonIgnore]
        public byte[] PasswordSalt { get; set; }

        [NotMapped]
        public string Token { get; set; }
    }
}
