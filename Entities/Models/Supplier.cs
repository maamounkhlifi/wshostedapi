﻿using Common.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entities.Models
{
    public partial class Supplier : IEntity
    {
        public Supplier()
        {
            Product = new HashSet<Product>();
        }

        public int? Id { get; set; }

        /// <summary>
        /// The company name of the supplier
        /// </summary>
        /// <example>Compa</example>
        [Required(ErrorMessage = "Company Name is required")]
        [StringLength(40)]
        public string CompanyName { get; set; }

        /// <summary>
        /// The contact name of the supplier
        /// </summary>
        /// <example>Conta</example>
        [Required(ErrorMessage = "Contact Name is required")]
        [StringLength(50)]
        public string ContactName { get; set; }

        /// <summary>
        /// The contat title of the supplier
        /// </summary>
        /// <example>Contitl</example>
        [StringLength(40)]
        public string ContactTitle { get; set; }

        /// <summary>
        /// The city of the supplier
        /// </summary>
        /// <example>Tunis</example>
        [Required]
        [StringLength(40)]
        public string City { get; set; }

        /// <summary>
        /// The country of the supplier
        /// </summary>
        /// <example>Tunisia</example>
        [Required]
        [StringLength(40)]
        public string Country { get; set; }

        /// <summary>
        /// The phone number of the supplier
        /// </summary>
        /// <example>25941505</example>
        [Required]
        [StringLength(30)]
        public string Phone { get; set; }

        /// <summary>
        /// The fax numberof the supplier
        /// </summary>
        /// <example>54540724</example>
        [StringLength(30)]
        public string Fax { get; set; }

        [SwaggerExclude]
        public virtual ICollection<Product> Product { get; set; }
    }
}
