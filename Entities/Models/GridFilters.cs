﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models
{
    public class GridFilter<T>
    {
        int pageNumber;
        int pageSize;
        string sortField;
        int sortOrder;

        public int PageNumber { get => pageNumber; set => pageNumber = value; }
        public int PageSize { get => pageSize; set => pageSize = value; }
        public string SortField { get => sortField; set => sortField = value; }
        public int SortOrder { get => sortOrder; set => sortOrder = value; }

        public T filter;
    }
}
