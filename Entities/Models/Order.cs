﻿using Common.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entities.Models
{
    public partial class Order : IEntity
    {
        public Order()
        {
            OrderItem = new HashSet<OrderItem>();
        }
       
        public int? Id { get; set; }

        /// <summary>
        /// The date of the order
        /// </summary>
        /// <example>15/05/2019</example>
        [Required(ErrorMessage = "Order Date is required")]
        [DataType(DataType.Date)]
        public DateTime? OrderDate { get; set; }

        /// <summary>
        /// The number of the order
        /// </summary>
        /// <example>5263</example>
        [Required]  
        [StringLength(10)]
        public string OrderNumber { get; set; }

        /// <summary>
        /// The customer id of the order
        /// </summary>
        /// <example>1</example>
        [Required(ErrorMessage = "Customer is required")]
        public int? CustomerId { get; set; }

        /// <summary>
        /// The total amount of the order
        /// </summary>
        /// <example>200</example>
        [Required(ErrorMessage = "Total Amount is required")]
        public decimal? TotalAmount { get; set; }

        [SwaggerExclude]
        public virtual Customer Customer { get; set; }

        [SwaggerExclude]
        public virtual ICollection<OrderItem> OrderItem { get; set; }
    }
}
