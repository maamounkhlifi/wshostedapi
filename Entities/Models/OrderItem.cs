﻿using Common.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entities.Models
{
    public partial class OrderItem : IEntity
    {
        public int? Id { get; set; }

        /// <summary>
        /// The id of the order
        /// </summary>
        /// <example>1</example>
        [Required(ErrorMessage = "Order is required")]
        public int? OrderId { get; set; }

        /// <summary>
        /// The id of the product
        /// </summary>
        /// <example>1</example>
        [Required(ErrorMessage = "Product is required")]        
        public int? ProductId { get; set; }

        /// <summary>
        /// The unit price of the order item
        /// </summary>
        /// <example>1</example>
        [Required(ErrorMessage = "Unit Price is required")]       
        public decimal? UnitPrice { get; set; }

        /// <summary>
        /// The quantity of the order item
        /// </summary>
        /// <example>1</example>
        [Required]        
        public int? Quantity { get; set; }

        [SwaggerExclude]
        public virtual Order Order { get; set; }

        [SwaggerExclude]
        public virtual Product Product { get; set; }
    }
}
