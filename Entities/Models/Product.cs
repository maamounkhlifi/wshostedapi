﻿using Common.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entities.Models
{
    public partial class Product : IEntity
    {
        public Product()
        {
            OrderItem = new HashSet<OrderItem>();
        }

        public int? Id { get; set; }

        /// <summary>
        /// The name of the product
        /// </summary>
        /// <example>Prod</example>
        [Required(ErrorMessage = "Product Name is required")]
        [StringLength(50)]
        public string ProductName { get; set; }

        /// <summary>
        /// The supplier id of the product
        /// </summary>
        /// <example>1</example>
        [Required(ErrorMessage = "Supplier is required")]
        public int? SupplierId { get; set; }

        /// <summary>
        /// The unit price of the product
        /// </summary>
        /// <example>5</example>
        [Required(ErrorMessage = "Unit Price is required")]
        public decimal? UnitPrice { get; set; }

        /// <summary>
        /// The package of the product
        /// </summary>
        /// <example></example>
        [StringLength(30)]
        public string Package { get; set; }

        /// <summary>
        /// The availability of the product
        /// </summary>
        /// <example>true</example>
        public bool? IsDiscontinued { get; set; }

        [SwaggerExclude]
        public virtual Supplier Supplier { get; set; }

        [SwaggerExclude]
        public virtual ICollection<OrderItem> OrderItem { get; set; }
    }
}
