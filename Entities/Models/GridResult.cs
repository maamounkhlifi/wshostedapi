﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models
{
    public class GridResult<T>
    {

        int totalRecords;
        IEnumerable<T> data;

        public int TotalRecords { get => totalRecords; set => totalRecords = value; }
        public IEnumerable<T> Data { get => data; set => data = value; }
    }
}
