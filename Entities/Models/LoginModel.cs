﻿using Common.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    public class LoginModel
    {        
        /// <summary>
        /// The username of the user
        /// </summary>
        /// <example>mkhlifi</example>
        [Required(ErrorMessage = "Username is required")]
        [StringLength(40)]
        public string Username { get; set; }

        /// <summary>
        /// The password of the user
        /// </summary>
        /// <example>password!*</example>
        [Required(ErrorMessage = "Password is required")]
        [StringLength(40)]
        [NotMapped]
        public string Password { get; set; }        
    }
}
