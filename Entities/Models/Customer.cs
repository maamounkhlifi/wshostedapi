﻿using Common.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entities.Models
{
    public partial class Customer : IEntity
    {
        public Customer()
        {
            Order = new HashSet<Order>();
        }

        public int? Id { get; set; }

        /// <summary>
        /// The name of the customer
        /// </summary>
        /// <example>Maamoun</example>
        [Required(ErrorMessage = "First Name is required")]
        [StringLength(40)]
        public string FirstName { get; set; }

        /// <summary>
        /// The last name of the customer
        /// </summary>
        /// <example>Khlifi</example>
        [Required(ErrorMessage = "Last Name is required")]
        [StringLength(40)]
        public string LastName { get; set; }

        /// <summary>
        /// The city name of the customer
        /// </summary>
        /// <example>Ezzahra</example>
        [Required]
        [StringLength(40)]
        public string City { get; set; }

        /// <summary>
        /// The country name of the customer
        /// </summary>
        /// <example>Tunisia</example>
        [Required]
        [StringLength(40)]
        public string Country { get; set; }

        /// <summary>
        /// The phone nmber of the customer
        /// </summary>
        /// <example>Khlifi</example>
        [Required]
        [StringLength(20)]
        public string Phone { get; set; }
        
        [SwaggerExclude]
        public virtual ICollection<Order> Order { get; set; }
    }
}
