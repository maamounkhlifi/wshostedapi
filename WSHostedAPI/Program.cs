﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using WSHostedAPI.Extensions;

namespace WSHostedAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // if debugger is attached the API runs on iis express                
            if (Debugger.IsAttached)
            {
                Debugger.Launch();
                CreateWebHostBuilder(args, Directory.GetCurrentDirectory()).Build().Run();
            }
            // else it runs as windows service
            else
            {
                // getting publish directory for windows service
                var pathToExe = Process.GetCurrentProcess().MainModule.FileName;
                var pathToContentRoot = Path.GetDirectoryName(pathToExe);
                CreateWebHostBuilder(args, pathToContentRoot).Build().RunAsCustomService();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args, string pathToContentRoot) =>
            WebHost.CreateDefaultBuilder(args)
                .UseContentRoot(pathToContentRoot)
                .UseStartup<Startup>()
                .UseIISIntegration()
                .UseIIS()
                .UseUrls("http://localhost:56226");
    }
}
