var webpack = require('webpack');
var appsettings = require('./appsettings.json');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');

var config = (env, options) => {
    let isProduction;

    if (env && env.NODE_ENV && env.NODE_ENV !== 'development') {
        isProduction = true;
    } else if (options && options.mode === 'production') {
        isProduction = true;
    } else {
        isProduction = false;
    }

    return {
        mode: isProduction ? 'production' : 'development',
        devtool: isProduction ? 'source-map' : 'cheap-module-source-map',
        entry: { 'main': './wwwroot/src/index.jsx' },
        resolve: {
            extensions: ['.js', '.jsx']
        },
        module: {
            rules: [
                {
                    test: /\.jsx?$/,
                    loader: 'babel-loader'
                },
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: [
                        {
                            loader: MiniCssExtractPlugin.loader,
                            options: {                                
                                hmr: !isProduction
                            },
                        },
                        "css-loader", "sass-loader"
                    ]
                },
                {
                    test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '[name].[ext]',
                                outputPath: 'fonts/'
                            }
                        }
                    ]
                },
                {
                    test: /\.(png|jpe?g|gif)$/i,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '[name].[ext]',
                                outputPath: 'img/'
                            }
                        },
                    ],
                },
            ]
        },
        resolve: {
            extensions: ['.js', '.jsx'],
            alias: {
                '@': path.resolve(__dirname, 'wwwroot/src/'),
            }
        },
        output: {
            path: path.resolve(__dirname, 'wwwroot/dist'),
            filename: 'main.js',
            publicPath: '/'
        },
        plugins: [
            new MiniCssExtractPlugin({
                // all options are optional
                filename: '[name].css'
            }),
            new HtmlWebpackPlugin({
                template: path.resolve(__dirname, 'wwwroot/src/') + '/index.html'
            }),
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
                Popper: ['popper.js', 'default']
            })
        ],
        /*optimization: {
            minimizer: [
                // we specify a custom UglifyJsPlugin here to get source maps in production
                new UglifyJsPlugin({
                    cache: true,
                    parallel: true,
                    uglifyOptions: {
                        compress: false,
                        ecma: 6,
                        mangle: true
                    },
                    sourceMap: true
                })
            ]
        },*/
        externals: {
            // global app config object
            config: JSON.stringify({
                apiUrl: appsettings.ApiConfig.Url + appsettings.ApiConfig.Version,
                audience: appsettings.Auth0.Audience,//'https://wshostedapi.captiva.com;',
                authority: appsettings.Auth0.Authority, //'https://wshostedapi.auth0.com',
                clientID: appsettings.Auth0.ClientID,//'3jfqd0knfU1d27WHWPKIHlJUXTjb3eEw',
                domain: appsettings.Auth0.Domain,//'wshostedapi.auth0.com',
                redirectUri: appsettings.Auth0.RedirectUri,//'http://localhost:56226/startSession'
            })
        }
    }
}
module.exports = config;