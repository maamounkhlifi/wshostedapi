﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;
using WSHostedAPI.Handlers;

namespace WSHostedAPI.Extensions
{
    public static class WebHostServiceExtensions
    {
        // Adding WebHostService events handler
        public static void RunAsCustomService(this IWebHost host)
        {
            var webHostService = new WebHostServiceHandler(host);
            ServiceBase.Run(webHostService);
        }
    }
}
