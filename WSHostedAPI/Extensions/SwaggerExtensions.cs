﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WSHostedAPI.Filters;

namespace WSHostedAPI.Extensions
{
    public static class SwaggerExtensions
    {
        public static void UseSwagger(this IApplicationBuilder app, ILogger logger)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v2/swagger.json", "Versioned Api v2");
                c.SwaggerEndpoint("/swagger/v1.1/swagger.json", "Versioned Api v1.1");
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Versioned Api v1");
            });
        }

        public static void ConfigureSwaggerGen(this IServiceCollection services)
        {
            services.AddSwaggerGen(o =>
           {

               o.SwaggerDoc("v2", new Info
               {
                   Version = "v2",
                   Title = "WSHosted API 2",
                   Description = "WSHosted API",
                   TermsOfService = "None",
                   Contact = new Contact { Name = "Maamoun Khlifi", Email = "maamoun.khlifi@gmail.com" },
                   License = new License { Name = "Use under LICX" }
               });

               o.SwaggerDoc("v1.1", new Info
               {
                   Version = "v1.1",
                   Title = "WSHosted API 1.1",
                   Description = "WSHosted API Test ",
                   TermsOfService = "None",
                   Contact = new Contact { Name = "Maamoun Khlifi", Email = "maamoun.khlifi@gmail.com" },
                   License = new License { Name = "Use under LICX" }
               });

               o.SwaggerDoc("v1", new Info
               {
                   Version = "v1",
                   Title = "WSHosted API 1",
                   Description = "WSHosted API Test ",
                   TermsOfService = "None",
                   Contact = new Contact { Name = "Maamoun Khlifi", Email = "maamoun.khlifi@gmail.com" },
                   License = new License { Name = "Use under LICX" }
               });


               // Add a custom filter for settint the default values  
               o.OperationFilter<SwaggerOperationFilter>();


               o.EnableAnnotations();

               // Set the comments path for the Swagger JSON and UI.
               var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
               var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
               o.IncludeXmlComments(xmlPath);

               o.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
               o.SchemaFilter<SwaggerExcludeFilter>();

           });
        }

    }
}
