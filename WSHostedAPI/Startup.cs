﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ActionFilters;
using Common;
using Contracts;
using Contracts.Services;
using Entities;
using Entities.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Logging;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using WSHostedAPI.Extensions;

namespace WSHostedAPI
{
    public class Startup
    {
        public Startup(ILogger<Startup> logger, IConfiguration configuration)
        {
            _logger = logger;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        private readonly ILogger<Startup> _logger;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            IdentityModelEventSource.ShowPII = true;
            services.AddSingleton<ISingleton, Singleton>();
            services.ConfigureCors();
            services.ConfigureIISIntegration();
            services.ConfigureDBContext(Configuration);
            services.ConfigureSwaggerGen();
            services.ConfigureAuthentication(Configuration);
            services.InjectRepositories();
            services.InjectServices();
            services.AddScoped(typeof(ValidateEntityExists<>));
            services.AddScoped<ValidationFilter>();
            services.AddMvc()
                .AddJsonOptions(options =>
                {
                    // Fixing self reference loop serialization issue
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options =>
                {
                    // Formatting JSON results
                    options.SerializerSettings.Formatting = Formatting.Indented;
                });
            services.ConfigureVersionning();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    EnvParam = new { NODE_ENV = "development" },
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.ConfigureExceptionHandler(_logger);
            }
            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                name: "api",
                template: "api/v{version:apiVersion}/[controller]");

                routes.MapRoute("NotFound",
                "404",
                new { controller = "Home", action = "Index" });

                routes.MapRoute("startSession",
                "startSession",
                new { controller = "Home", action = "Index" });

                routes.MapRoute(
                name: "default",
                template: "Home/{*url}",
                defaults: new { controller = "Home", action = "Index" });

            });
            app.Use(async (context, next) =>
            {
                await next();

                if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value) &&
                    !context.Request.Path.Value.StartsWith("/api"))
                {
                    context.Response.Redirect("/404");
                }
            });
            app.UseSwagger(_logger);

            // Creating a file for log
            loggerFactory.AddFile("%SYSTEMDRIVE%\\Logs\\WSHostedAPI-{Date}.txt");

        }
    }
}
