﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace WSHostedAPI.Controllers
{
    public class HomeController : Controller
    {
        IHostingEnvironment _hostingEnv;
        public HomeController(ISingleton singleton, IHostingEnvironment hostingEnv)
        {
            _hostingEnv = hostingEnv;
        }
        public IActionResult Index()
        {
            string contentRootPath = _hostingEnv.ContentRootPath;
            var content = System.IO.File.ReadAllText(contentRootPath + "/wwwroot/dist/index.html");
            return new ContentResult
            {
                ContentType = "text/html",
                StatusCode = (int)HttpStatusCode.OK,
                Content = content
            };
        }
        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }
    }
}