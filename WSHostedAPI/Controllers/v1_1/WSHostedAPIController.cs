﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;

namespace WSHostedAPI.Controllers.v1_1
{
    [ApiVersion("1.1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class WSHostedAPIController : ControllerBase
    {
        WSHostedAPIContext _wshostedAPIContext;
        private readonly ILogger _logger;
        private IRepositoryWrapper _wshostedapiWrapper;

        public WSHostedAPIController(WSHostedAPIContext wshostedAPIContext, ILogger<WSHostedAPIController> logger, IRepositoryWrapper wshostedapiWrapper)
        {
            _wshostedAPIContext = wshostedAPIContext;
            _logger = logger;
            _wshostedapiWrapper = wshostedapiWrapper;

        }

        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/wshostedapi
        ///     {
        ///        
        ///     }
        ///
        /// </remarks>        
        [HttpGet()]
        [SwaggerOperation(Summary = "Check API Status", Description = "Public")]
        [SwaggerResponse(200, "API Alive")]
        public IActionResult Get()
        {
            return Ok("I'am Alive! v1.1");
        }

    }
}
