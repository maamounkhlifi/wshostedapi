﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entities.Models;
using Microsoft.Extensions.Logging;
using Contracts;
using Entities.Extensions;
using ActionFilters;
using Contracts.Services;
using Microsoft.AspNetCore.Authorization;

namespace WSHostedAPI.Controllers.v1_1
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class OrderItemsController : ControllerBase
    {
        private readonly WSHostedAPIContext _wshostedAPIContext;
        private readonly ILogger _logger;
        private IOrderItemService _orderItemService;

        public OrderItemsController(WSHostedAPIContext wshostedAPIContext, ILogger<OrderItemsController> logger, IOrderItemService orderItemService)
        {
            _wshostedAPIContext = wshostedAPIContext;
            _logger = logger;
            _orderItemService = orderItemService;
        }

        // GET: api/OrderItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrderItem>>> GetOrderItem()
        {
            return Ok(await _orderItemService.FindAllAsync());
        }

        // GET: api/OrderItems/5
        [HttpGet("{id}")]
        [ServiceFilter(typeof(ValidateEntityExists<OrderItem>))]
        public async Task<ActionResult<OrderItem>> GetOrderItem(int id)
        {
        
            var orderItem = await _orderItemService.FindByConditionAsync(p => p.Id == id, o => o.Order, o => o.Product);

            return Ok(orderItem.FirstOrDefault());
        }

        // PUT: api/OrderItems/5
        [HttpPut("{id}")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> PutOrderItem(int id, OrderItem orderItem)
        {
            if (id != orderItem.Id)
            {
                return BadRequest();
            }

            _orderItemService.Update(orderItem);

            try
            {
                await _orderItemService.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderItemExists(id).Result)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/OrderItems
        [HttpPost]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<ActionResult<OrderItem>> PostOrderItem(OrderItem orderItem)
        {
            
            _orderItemService.Create(orderItem);
            await _orderItemService.Save();

            return CreatedAtAction("PostOrderItem", new { id = orderItem.Id }, orderItem);
        }

        // DELETE: api/OrderItems/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<OrderItem>> DeleteOrderItem(int id)
        {
            var orderItem = await _orderItemService.FindByConditionAsync(p => p.Id == id);
            if (orderItem.Count() == 0)
            {
                return NotFound();
            }

            _orderItemService.Delete(orderItem.First());
            await _orderItemService.Save();

            return orderItem.First();
        }

        // PUT: api/OrderItems
        [HttpPut]
        public async Task<IActionResult> PutOrderItemGrid(GridFilter<OrderItem> gridFilter)
        {
            if (gridFilter == null || gridFilter.PageNumber < 0 || gridFilter.PageSize <= 0)
            {
                return BadRequest();
            }

            var gridResult = await _orderItemService.FindGridDataAsync(gridFilter);

            return Ok(gridResult);
        }

        private async Task<bool> OrderItemExists(int id)
        {
            return await _wshostedAPIContext.OrderItem.AnyAsync(e => e.Id == id);
        }
    }
}
