﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entities.Models;
using Microsoft.Extensions.Logging;
using Contracts;
using Entities.Extensions;
using ActionFilters;
using Contracts.Services;
using Microsoft.AspNetCore.Authorization;

namespace WSHostedAPI.Controllers.v1_1
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class SuppliersController : ControllerBase
    {
        private readonly WSHostedAPIContext _wshostedAPIContext;
        private readonly ILogger _logger;
        private ISupplierService _supplierService;

        public SuppliersController(WSHostedAPIContext wshostedAPIContext, ILogger<SuppliersController> logger, ISupplierService supplierService)
        {
            _wshostedAPIContext = wshostedAPIContext;
            _logger = logger;
            _supplierService = supplierService;
        }

        // GET: api/Suppliers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Supplier>>> GetSupplier()
        {
            return Ok(await _supplierService.FindAllAsync());
        }

        // GET: api/Suppliers/5
        [HttpGet("{id}")]
        [ServiceFilter(typeof(ValidateEntityExists<Supplier>))]
        public ActionResult<Supplier> GetCustomer(int id)
        {
            var supplier = _supplierService.FindByConditionAsync(p => p.Id == id).Result.FirstOrDefault();

            return Ok(supplier);
        }

        // PUT: api/Suppliers/5
        [HttpPut("{id}")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> PutSupplier(int id, Supplier supplier)
        {
            if (id != supplier.Id)
            {
                return BadRequest();
            }

            _supplierService.Update(supplier);

            try
            {
                await _supplierService.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SupplierExists(id).Result)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Suppliers
        [HttpPost]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<ActionResult<Supplier>> PostSupplier(Supplier supplier)
        {                        

            _supplierService.Create(supplier);
            await _supplierService.Save();

            return CreatedAtAction("PostSupplier", new { id = supplier.Id }, supplier);
        }

        // DELETE: api/Suppliers/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Supplier>> DeleteSupplier(int id)
        {
            var supplier = await _supplierService.FindByConditionAsync(p => p.Id == id);
            if (supplier.Count() == 0)
            {
                return NotFound();
            }

            _supplierService.Delete(supplier.First());
            await _supplierService.Save();

            return supplier.First();
        }

        // PUT: api/Suppliers
        [HttpPut]
        public async Task<IActionResult> PutSupplierGrid(GridFilter<Supplier> gridFilter)
        {
            if (gridFilter == null || gridFilter.PageNumber < 0 || gridFilter.PageSize <= 0)
            {
                return BadRequest();
            }

            var gridResult = await _supplierService.FindGridDataAsync(gridFilter);

            return Ok(gridResult);
        }

        private async Task<bool> SupplierExists(int id)
        {
            return await _wshostedAPIContext.Supplier.AnyAsync(e => e.Id == id);
        }
    }
}
