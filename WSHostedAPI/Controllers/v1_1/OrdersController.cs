﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entities.Models;
using Microsoft.Extensions.Logging;
using Contracts;
using Entities.Extensions;
using ActionFilters;
using Contracts.Services;
using Microsoft.AspNetCore.Authorization;

namespace WSHostedAPI.Controllers.v1_1
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class OrdersController : ControllerBase
    {
        private readonly WSHostedAPIContext _wshostedAPIContext;
        private readonly ILogger _logger;
        private IOrderService _orderService;

        public OrdersController(WSHostedAPIContext wshostedAPIContext, ILogger<OrdersController> logger, IOrderService orderService)
        {
            _wshostedAPIContext = wshostedAPIContext;
            _logger = logger;
            _orderService = orderService;
        }

        // GET: api/Orders
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Order>>> GetOrder()
        {
            return Ok(await _orderService.FindAllAsync());
        }

        // GET: api/Orders/5
        [HttpGet("{id}")]
        [ServiceFilter(typeof(ValidateEntityExists<Order>))]
        public ActionResult<Order> GetOrder(int id)
        {
            var order = _orderService.FindByConditionAsync(p => p.Id == id, p => p.Customer, p => p.OrderItem).Result.FirstOrDefault();

            return Ok(order);
        }

        // PUT: api/Orders/5
        [HttpPut("{id}")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> PutOrder(int id, Order order)
        {
            if (id != order.Id)
            {
                return BadRequest();
            }

            _orderService.Update(order);

            try
            {
                await _orderService.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(id).Result)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Orders
        [HttpPost]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<ActionResult<Order>> PostOrder(Order order)
        {            

            _orderService.Create(order);
            await _orderService.Save();

            return CreatedAtAction("PostOrder", new { id = order.Id }, order);
        }

        // DELETE: api/Orders/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Order>> DeleteOrder(int id)
        {
            var order = await _orderService.FindByConditionAsync(p => p.Id == id);
            if (order.Count() == 0)
            {
                return NotFound();
            }

            _orderService.Delete(order.First());
            await _orderService.Save();

            return order.First();
        }

        // PUT: api/Orders
        [HttpPut]
        public async Task<IActionResult> PutOrderGrid(GridFilter<Order> gridFilter)
        {
            if (gridFilter == null || gridFilter.PageNumber < 0 || gridFilter.PageSize <= 0)
            {
                return BadRequest();
            }

            var gridResult = await _orderService.FindGridDataAsync(gridFilter);

            return Ok(gridResult);
        }

        private async Task<bool> OrderExists(int id)
        {
            return await _wshostedAPIContext.Order.AnyAsync(e => e.Id == id);
        }
    }
}
