﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entities.Models;
using Microsoft.Extensions.Logging;
using Contracts;
using Entities.Extensions;
using ActionFilters;
using Contracts.Services;
using Microsoft.AspNetCore.Authorization;

namespace WSHostedAPI.Controllers.v1_1
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class CustomersController : ControllerBase
    {
        private readonly WSHostedAPIContext _wshostedAPIContext;
        private readonly ILogger _logger;
        private ICustomerService _customerService;

        public CustomersController(WSHostedAPIContext wshostedAPIContext, ILogger<CustomersController> logger, ICustomerService customerService)
        {
            _wshostedAPIContext = wshostedAPIContext;
            _logger = logger;
            _customerService = customerService;
        }

        // GET: api/Customers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Customer>>> GetCustomer()
        {            
            return Ok(await _customerService.FindAllAsync());         
        }

        // GET: api/Customers/5
        [HttpGet("{id}")]
        [ServiceFilter(typeof(ValidateEntityExists<Customer>))]
        public ActionResult<Customer> GetCustomer(int id)
        {
            var customer = _customerService.FindByConditionAsync(p => p.Id == id, p=> p.Order).Result.FirstOrDefault();

            return Ok(customer);
        }

        // PUT: api/Customers/5
        [HttpPut("{id}")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> PutCustomer(int id, Customer customer)
        {
            if (id != customer.Id)
            {
                return BadRequest();
            }

            _customerService.Update(customer);

            try
            { 
                await _customerService.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomerExists(id).Result)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Customers
        [HttpPost]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<ActionResult<Customer>> PostCustomer([FromBody]Customer customer)
        {                      

            _customerService.Create(customer);
            await _customerService.Save();

            return CreatedAtAction("PostCustomer", new { id = customer.Id }, customer);
        }

        // DELETE: api/Customers/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Customer>> DeleteCustomer(int id)
        {
            var customer = await _customerService.FindByConditionAsync(p => p.Id == id);
            if (customer.Count() == 0)
            {
                return NotFound();
            }

            _customerService.Delete(customer.First());
            await _customerService.Save();

            return customer.First();
        }

        // PUT: api/Customers
        [HttpPut]
        public async Task<IActionResult> PutCustomerGrid(GridFilter<Customer> gridFilter)
        {
            if (gridFilter == null || gridFilter.PageNumber < 0 || gridFilter.PageSize <= 0)
            {
                return BadRequest();
            }

            var gridResult = await _customerService.FindGridDataAsync(gridFilter);
         
            return Ok(gridResult);
        }

        // PUT: api/Customers
        [HttpGet("GetAllRecordsForField/{query}/{field}")]
        public IActionResult GetAllRecordsForField(string query, string field)
        {
            if (string.IsNullOrWhiteSpace(query))
            {
                return BadRequest();
            }

            var result = _customerService.GetAllRecordsForField(query, field);

            return Ok(result);
        }

        private async Task<bool> CustomerExists(int id)
        {
            return await _wshostedAPIContext.Customer.AnyAsync(e => e.Id == id);            
        }
    }
}
