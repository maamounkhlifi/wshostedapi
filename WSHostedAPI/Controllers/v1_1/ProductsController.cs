﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entities.Models;
using Microsoft.Extensions.Logging;
using Contracts;
using Entities.Extensions;
using ActionFilters;
using Contracts.Services;
using Microsoft.AspNetCore.Authorization;

namespace WSHostedAPI.Controllers.v1_1
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class ProductsController : ControllerBase
    {
        private readonly WSHostedAPIContext _wshostedAPIContext;
        private readonly ILogger _logger;
        private IProductService _productService;

        public ProductsController(WSHostedAPIContext wshostedAPIContext, ILogger<ProductsController> logger, IProductService productService)
        {
            _wshostedAPIContext = wshostedAPIContext;
            _logger = logger;
            _productService = productService;
        }

        // GET: api/Products
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product>>> GetProduct()
        {
            return Ok(await _productService.FindAllAsync());
        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        [ServiceFilter(typeof(ValidateEntityExists<Product>))]
        public ActionResult<Product> GetProduct(int id)
        {
            var product = _productService.FindByConditionAsync(p => p.Id == id, pr => pr.Supplier).Result.FirstOrDefault();

            return Ok(product);
        }

        // PUT: api/Products/5
        [HttpPut("{id}")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> PutProduct(int id, Product product)
        {
            if (id != product.Id)
            {
                return BadRequest();
            }

            _productService.Update(product);

            try
            {
                await _productService.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id).Result)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Products
        [HttpPost]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<ActionResult<Product>> PostProduct(Product product)
        {            

            _productService.Create(product);
            await _productService.Save();

            return CreatedAtAction("PostProduct", new { id = product.Id }, product);
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Product>> DeleteProduct(int id)
        {
            var product = await _productService.FindByConditionAsync(p => p.Id == id);
            if (product.Count() == 0)
            {
                return NotFound();
            }

            _productService.Delete(product.First());
            await _productService.Save();

            return product.First();
        }

        // PUT: api/Products
        [HttpPut]
        public async Task<IActionResult> PutProductGrid(GridFilter<Product> gridFilter)
        {
            if (gridFilter == null || gridFilter.PageNumber < 0 || gridFilter.PageSize <= 0)
            {
                return BadRequest();
            }

            var gridResult = await _productService.FindGridDataAsync(gridFilter);

            return Ok(gridResult);
        }

        private async Task<bool> ProductExists(int id)
        {
            return await _wshostedAPIContext.Product.AnyAsync(e => e.Id == id);
        }
    }
}
