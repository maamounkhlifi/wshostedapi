﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ActionFilters;
using Contracts;
using Contracts.Services;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;

namespace WSHostedAPI.Controllers.v2
{
    [ApiVersion("2")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        WSHostedAPIContext _wshostedAPIContext;
        private readonly ILogger _logger;
        private IUserService _userService;

        public UsersController(WSHostedAPIContext wshostedAPIContext, ILogger<UsersController> logger, IUserService userService)
        {
            _wshostedAPIContext = wshostedAPIContext;
            _logger = logger;
            _userService = userService;
        }
       
        [AllowAnonymous]
        [SwaggerOperation(Summary = "Authentificate users", Description = "Login")]
        [SwaggerResponse(200, "User Authentificated")]
        [SwaggerResponse(400, "Invalid User")]
        [SwaggerResponse(401, "Unauthorized User")]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]LoginModel userParam)
        {
            var user = _userService.Authenticate(userParam);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(user);
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetUser()
        {
            return Ok(await _userService.FindAllAsync());
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        [ServiceFilter(typeof(ValidateEntityExists<User>))]
        public ActionResult<User> GetUser(int id)
        {
            var user = _userService.FindByConditionAsync(p => p.Id == id).Result.FirstOrDefault();

            return Ok(user);
        }

        // POST: api/Users
        [HttpPost]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<ActionResult<User>> PostUser([FromBody]User user)
        {

            _userService.Create(user);
            await _userService.Save();

            return CreatedAtAction("PostUser", new { id = user.Id }, user);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        [ServiceFilter(typeof(ValidationFilter))]
        public async Task<IActionResult> PutCustomer(int id, User user)
        {
            if (id != user.Id)
            {
                return BadRequest();
            }

            _userService.Update(user);

            try
            {
                await _userService.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id).Result)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // DELETE: api/User/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<User>> DeleteUser(int id)
        {
            var user = await _userService.FindByConditionAsync(p => p.Id == id);
            if (user.Count() == 0)
            {
                return NotFound();
            }

            _userService.Delete(user.First());
            await _userService.Save();

            return user.First();
        }

        private async Task<bool> UserExists(int id)
        {
            return await _wshostedAPIContext.User.AnyAsync(e => e.Id == id);
        }
    }
}
