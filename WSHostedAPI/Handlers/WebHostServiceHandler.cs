﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSHostedAPI.Handlers
{
    public class WebHostServiceHandler : WebHostService
    {
        private readonly ILogger _logger;

        public WebHostServiceHandler(IWebHost host) : base(host)
        {
            _logger = host.Services.GetRequiredService<ILogger<WebHostServiceHandler>>();

        }

        protected override void OnStarting(string[] args)
        {
            _logger.LogInformation("Windows Service is starting");
            System.Diagnostics.Debugger.Launch();
            base.OnStarting(args);
        }

        protected override void OnStarted()
        {
            base.OnStarted();
            _logger.LogInformation("Windows Service started");
        }

        protected override void OnStopping()
        {
            base.OnStopping();
            _logger.LogInformation("Windows Service stopped");
        }
    }
}
