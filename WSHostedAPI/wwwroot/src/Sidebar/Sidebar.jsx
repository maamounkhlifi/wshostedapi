import React from "react";
import { Link } from "react-router-dom";
import Menu from './Menu';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { store } from '../_helpers';
import { action as toggleMenu } from 'redux-burger-menu';
import { history } from '../_helpers';

class Sidebar extends React.Component {
    constructor(props) {
        super(props);
    }

    logout() {
        this.props.dispatch(userActions.logout());
        const isOpen = false;
        store.dispatch(toggleMenu(isOpen));
    }

    render() {
        return (
            <Menu >
                <Link className="menu-item" to="/" >Home</Link>
                <Link className="menu-item" to="/Home/Customers" >Customers</Link>
                <Link className="menu-item" to="/Home/Orders" >Orders</Link>
                <Link className="menu-item" to="/Home/Suppliers" >Suppliers</Link>
                <Link className="menu-item" to="/Home/Products" >Products</Link>
                <a onClick={() => this.logout()}>Logout</a>
            </Menu>
        )
    };
};

function mapStateToProps(state) {
    return {
    };
}

const connectedSidebar = connect(mapStateToProps)(Sidebar);
export { connectedSidebar as Sidebar };