import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { authenticationService } from '@/_services';

export const PublicRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => {
        if (authenticationService.isAuthenticated()) {
            // logged in so redirect to home page with the return url
            return <Redirect to={{ pathname: '/', state: { from: props.location } }} />
        }

        // unauthorised so return component
        return <Component {...props} />
    }} />
)