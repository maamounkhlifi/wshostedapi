import React from 'react';
import { CustomersGrid } from '../_grid';

class Customers extends React.Component {
    constructor(props) {
        super(props);

    }    

    componentDidMount() {
    }

    render() {
        return (
            <div>
                Customers                
                <CustomersGrid></CustomersGrid>
            </div>
        );
    }
}

export { Customers };