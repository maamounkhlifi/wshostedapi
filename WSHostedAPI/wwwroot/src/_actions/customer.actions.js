import { customerConstants } from '../_constants';
import { CustomerService } from '../_services';
import { alertActions } from './';
import { store } from '../_helpers';

export const customerActions = {
    getAll,
    getFilteredData,
    update,
    add,
    remove
};

function getAll() {
    var customerService = new CustomerService();
    return dispatch => {
        dispatch(request());

        customerService.getAll()
            .then(
                customers => dispatch(success(customers)),
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: customerConstants.GETALL_REQUEST } }
    function success(customers) { return { type: customerConstants.GETALL_SUCCESS, customers } }
    function failure(error) { return { type: customerConstants.GETALL_FAILURE, error } }
}

function getFilteredData(filters) {
    var customerService = new CustomerService();
    return dispatch => {
        dispatch(request());  
              
        customerService.getFilteredData(filters)
            .then(
                gridResult => {
                    if(store.getState().customers.totalRecords !== gridResult.totalRecords)
                        filters.pageNumber = 0;
                        dispatch(success({ ...gridResult, pageSize: filters.pageSize, pageNumber: filters.pageNumber, sortField: filters.sortField, sortOrder: filters.sortOrder}))
                    },
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: customerConstants.GETFILTEREDDATA_REQUEST } }
    function success(results) { return { type: customerConstants.GETFILTEREDDATA_SUCCESS, ...results } }
    function failure(error) { return { type: customerConstants.GETFILTEREDDATA_FAILURE, error } }
}

function update(customer, id) {
    var customerService = new CustomerService();
    return dispatch => {
        dispatch(request());

        customerService.update(customer, id)
            .then(
                response => {
                    dispatch(success(response));
                },
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: customerConstants.UPDATE_REQUEST } }
    function success(response) { return { type: customerConstants.UPDATE_SUCCESS, response } }
    function failure(error) { return { type: customerConstants.UPDATE_FAILURE, error } }
}

function add(customer) {
    var customerService = new CustomerService();
    return dispatch => {
        dispatch(request());

        customerService.add(customer)
            .then(
                response => {
                    dispatch(success(response));
                },
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: customerConstants.ADD_REQUEST } }
    function success(response) { return { type: customerConstants.ADD_SUCCESS, response } }
    function failure(error) { return { type: customerConstants.ADD_FAILURE, error } }
}

function remove(id) {
    var customerService = new CustomerService();
    return dispatch => {
        dispatch(request());

        customerService.remove(id)
            .then(
                response => {
                    dispatch(success(response));
                },
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: customerConstants.REMOVE_REQUEST } }
    function success(response) { return { type: customerConstants.REMOVE_SUCCESS, response } }
    function failure(error) { return { type: customerConstants.REMOVE_FAILURE, error } }
}