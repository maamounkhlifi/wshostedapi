import { supplierConstants } from '../_constants';
import { SupplierService } from '../_services';
import { alertActions } from './';
import { store } from '../_helpers';

export const supplierActions = {
    getAll,
    getFilteredData,
    update,
    add,
    remove   
};

function getAll() {
    var supplierService = new SupplierService();
    return dispatch => {
        dispatch(request());
        
        supplierService.getAll()
            .then(
                suppliers => dispatch(success(suppliers)),
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: supplierConstants.GETALL_REQUEST } }
    function success(suppliers) { return { type: supplierConstants.GETALL_SUCCESS, suppliers } }
    function failure(error) { return { type: supplierConstants.GETALL_FAILURE, error } }
}

function getFilteredData(filters) {
    var supplierService = new SupplierService();
    return dispatch => {
        dispatch(request());

        supplierService.getFilteredData(filters)
            .then(
                gridResult => {
                    if(store.getState().suppliers.totalRecords !== gridResult.totalRecords)
                        filters.pageNumber = 0;
                    dispatch(success({ ...gridResult, pageSize: filters.pageSize, pageNumber: filters.pageNumber, sortField: filters.sortField, sortOrder: filters.sortOrder}))
                },
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: supplierConstants.GETFILTEREDDATA_REQUEST } }
    function success(results) { return { type: supplierConstants.GETFILTEREDDATA_SUCCESS, ...results } }
    function failure(error) { return { type: supplierConstants.GETFILTEREDDATA_FAILURE, error } }
}

function update(supplier, id) {
    var supplierService = new SupplierService();
    return dispatch => {
        dispatch(request());
        
        supplierService.update(supplier, id)
            .then(
                response => {                                        
                    dispatch(success(response));
                },
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: supplierConstants.UPDATE_REQUEST } }
    function success(response) { return { type: supplierConstants.UPDATE_SUCCESS, response } }
    function failure(error) { return { type: supplierConstants.UPDATE_FAILURE, error } }
}

function add(supplier) {
    var supplierService = new SupplierService();
    return dispatch => {
        dispatch(request());
        
        supplierService.add(supplier)
            .then(
                response => {                                        
                    dispatch(success(response));
                },
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: supplierConstants.ADD_REQUEST } }
    function success(response) { return { type: supplierConstants.ADD_SUCCESS, response } }
    function failure(error) { return { type: supplierConstants.ADD_FAILURE, error } }
}

function remove(id) {
    var supplierService = new SupplierService();
    return dispatch => {
        dispatch(request());
        
        supplierService.remove(id)
            .then(
                response => {                                        
                    dispatch(success(response));
                },
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: supplierConstants.REMOVE_REQUEST } }
    function success(response) { return { type: supplierConstants.REMOVE_SUCCESS, response } }
    function failure(error) { return { type: supplierConstants.REMOVE_FAILURE, error } }
}