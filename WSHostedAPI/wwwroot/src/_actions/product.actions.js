import { productConstants } from '../_constants';
import { ProductService } from '../_services';
import { alertActions } from './';
import { store } from '../_helpers';

export const productActions = {
    getAll,
    getFilteredData,
    update,
    add,
    remove     
};

function getAll() {
    var productService = new ProductService();
    return dispatch => {
        dispatch(request());
        
        productService.getAll()
            .then(
                products => dispatch(success(products)),
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: productConstants.GETALL_REQUEST } }
    function success(products) { return { type: productConstants.GETALL_SUCCESS, products } }
    function failure(error) { return { type: productConstants.GETALL_FAILURE, error } }
}

function getFilteredData(filters) {
    var productService = new ProductService();
    return dispatch => {
        dispatch(request());

        productService.getFilteredData(filters)
            .then(
                gridResult => {
                    if(store.getState().products.totalRecords !== gridResult.totalRecords)
                        filters.pageNumber = 0;
                        dispatch(success({ ...gridResult, pageSize: filters.pageSize, pageNumber: filters.pageNumber, sortField: filters.sortField, sortOrder: filters.sortOrder}))
                    },
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: productConstants.GETFILTEREDDATA_REQUEST } }
    function success(results) { return { type: productConstants.GETFILTEREDDATA_SUCCESS, ...results } }
    function failure(error) { return { type: productConstants.GETFILTEREDDATA_FAILURE, error } }
}

function update(product, id) {
    var productService = new ProductService();
    return dispatch => {
        dispatch(request());
        
        productService.update(product, id)
            .then(
                response => {                                        
                    dispatch(success(response));
                },
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: productConstants.UPDATE_REQUEST } }
    function success(response) { return { type: productConstants.UPDATE_SUCCESS, response } }
    function failure(error) { return { type: productConstants.UPDATE_FAILURE, error } }
}

function add(product) {
    var productService = new ProductService();
    return dispatch => {
        dispatch(request());
        
        productService.add(product)
            .then(
                response => {                                        
                    dispatch(success(response));
                },
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: productConstants.ADD_REQUEST } }
    function success(response) { return { type: productConstants.ADD_SUCCESS, response } }
    function failure(error) { return { type: productConstants.ADD_FAILURE, error } }
}

function remove(id) {
    var productService = new ProductService();
    return dispatch => {
        dispatch(request());
        
        productService.remove(id)
            .then(
                response => {                                        
                    dispatch(success(response));
                },
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: productConstants.REMOVE_REQUEST } }
    function success(response) { return { type: productConstants.REMOVE_SUCCESS, response } }
    function failure(error) { return { type: productConstants.REMOVE_FAILURE, error } }
}