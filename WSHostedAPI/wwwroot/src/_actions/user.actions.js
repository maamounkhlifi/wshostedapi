import { userConstants } from '../_constants';
import { userService, authenticationService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const userActions = {
    login,
    logout,
    getAll,
    loginAuth0
};

function login(username, password) {
    return dispatch => {
        dispatch(request({ username }));

        if (localStorage.getItem("authScheme") === "custom") {
            authenticationService.login(username, password)
                .then(
                    user => {
                        dispatch(success(user));
                        history.push('/');
                    },
                    error => {
                        dispatch(failure(error));
                        dispatch(alertActions.error(error));
                    }
                );
        }
        else
            authenticationService.login();
    };
    
    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function loginAuth0(error) {
    return dispatch => {
        if (error) {
            dispatch(failure(error));
            dispatch(alertActions.error(error));
        }
        else {
            let user = JSON.parse(sessionStorage.getItem('user'));
            if (user) {
                dispatch(success(user));
                history.push('/');
            }
        }
    }

    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout() {
    authenticationService.logout(history);
    return { type: userConstants.LOGOUT };
}

function getAll() {
    return dispatch => {
        dispatch(request());

        userService.getAll()
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            );
    };

    function request() { return { type: userConstants.GETALL_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_FAILURE, error } }
}