import { orderConstants } from '../_constants';
import { OrderService } from '../_services';
import { alertActions } from './';
import { store } from '../_helpers';

export const orderActions = {
    getAll,
    getFilteredData,
    update,
    add,
    remove     
};

function getAll() {
    var orderService = new OrderService();
    return dispatch => {
        dispatch(request());
        
        orderService.getAll()
            .then(
                orders => dispatch(success(orders)),
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: orderConstants.GETALL_REQUEST } }
    function success(orders) { return { type: orderConstants.GETALL_SUCCESS, orders } }
    function failure(error) { return { type: orderConstants.GETALL_FAILURE, error } }
}

function getFilteredData(filters) {
    var orderService = new OrderService();
    return dispatch => {
        dispatch(request());

        orderService.getFilteredData(filters)
            .then(
                gridResult => {
                    if(store.getState().orders.totalRecords !== gridResult.totalRecords)
                        filters.pageNumber = 0;
                        dispatch(success({ ...gridResult, pageSize: filters.pageSize, pageNumber: filters.pageNumber, sortField: filters.sortField, sortOrder: filters.sortOrder}))
                    },
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: orderConstants.GETFILTEREDDATA_REQUEST } }
    function success(results) { return { type: orderConstants.GETFILTEREDDATA_SUCCESS, ...results } }
    function failure(error) { return { type: orderConstants.GETFILTEREDDATA_FAILURE, error } }
}

function update(order, id) {
    var orderService = new OrderService();
    return dispatch => {
        dispatch(request());
        
        orderService.update(order, id)
            .then(
                response => {                                        
                    dispatch(success(response));
                },
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: orderConstants.UPDATE_REQUEST } }
    function success(response) { return { type: orderConstants.UPDATE_SUCCESS, response } }
    function failure(error) { return { type: orderConstants.UPDATE_FAILURE, error } }
}

function add(order) {
    var orderService = new OrderService();
    return dispatch => {
        dispatch(request());
        
        orderService.add(order)
            .then(
                response => {                                        
                    dispatch(success(response));
                },
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: orderConstants.ADD_REQUEST } }
    function success(response) { return { type: orderConstants.ADD_SUCCESS, response } }
    function failure(error) { return { type: orderConstants.ADD_FAILURE, error } }
}

function remove(id) {
    var orderService = new OrderService();
    return dispatch => {
        dispatch(request());
        
        orderService.remove(id)
            .then(
                response => {                                        
                    dispatch(success(response));
                },
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: orderConstants.REMOVE_REQUEST } }
    function success(response) { return { type: orderConstants.REMOVE_SUCCESS, response } }
    function failure(error) { return { type: orderConstants.REMOVE_FAILURE, error } }
}