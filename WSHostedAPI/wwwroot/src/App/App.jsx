import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';
import { history } from '@/_helpers';
import { authenticationService } from '@/_services';
import { alertActions } from '../_actions';
import { PublicRoute, PrivateRoute } from '@/_components';
import { HomePage } from '@/HomePage';
import { Customers } from '@/Customers';
import { Orders } from '@/Orders';
import { Products } from '@/Products';
import { Suppliers } from '@/Suppliers';
import { LoginPage } from '@/LoginPage';
import { NotFound } from '@/NotFound';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Sidebar } from '@/Sidebar';

class App extends React.Component {
    constructor(props) {
        super(props);
        Date.prototype.toJSON = function(){ return moment(this).format(); }
        const { dispatch } = this.props;
        history.listen((location, action) => {
            // clear alert on location change
            dispatch(alertActions.clear());
        });
    }

    startSession(history) {
        if(authenticationService.isAuthenticated())
            return;
        authenticationService.handleAuthentication(history);
        return <div><p>Starting session...</p></div>;
    }

    render() {
        const { alert, authentication } = this.props;
        return (
            <div>
                <Router history={history}>
                    {authentication.loggedIn && <Sidebar />}
                    <div className="jumbotron">
                        <div className="container">
                            <div className="col-sm-8 col-sm-offset-2 col-md-12">
                                {alert.message &&
                                    <div className={`alert ${alert.type}`}>{alert.message}</div>
                                }

                                <div>
                                    <Switch>
                                        <PrivateRoute exact path="/" component={HomePage} />
                                        <PublicRoute path="/Home/login" component={LoginPage} />
                                        <PrivateRoute exact path="/Home/Customers" component={Customers} />                                       
                                        <PrivateRoute exact path="/Home/Orders" component={Orders} />
                                        <PrivateRoute exact path="/Home/Suppliers" component={Suppliers} />
                                        <PrivateRoute exact path="/Home/Products" component={Products} />
                                        <Route path="/startSession" render={({ history }) => this.startSession(history)} />
                                        <Route exact path="/404" component={NotFound} />
                                        <Route component={NotFound} />
                                    </Switch>
                                </div>
                            </div>
                        </div>
                    </div>
                </Router>
            </div >
        );
    }
}

function mapStateToProps(state) {
    const { alert, authentication } = state;
    return {
        alert,
        authentication
    };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App };