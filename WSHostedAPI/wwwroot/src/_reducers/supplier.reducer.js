import { supplierConstants } from '../_constants';

export function suppliers(state = {}, action) {
  switch (action.type) {
    case supplierConstants.GETFILTEREDDATA_REQUEST:
      return {        
        ...state, loading: true
      };
    case supplierConstants.GETFILTEREDDATA_SUCCESS:
      return {
        ...state, suppliers: action.data, loading: false, totalRecords:  action.totalRecords, pageSize : action.pageSize, pageNumber: action.pageNumber,
        sortField: action.sortField, sortOrder: action.sortOrder
      };
    case supplierConstants.GETFILTEREDDATA_FAILURE:
      return {
        ...state, error: "Failed to load Suppliers grid Data", loading: false
      };
    case supplierConstants.GETALL_REQUEST:
      return {        
        ...state, loading: true
      };
    case supplierConstants.GETALL_SUCCESS:
      return {
        ...state, suppliers: action.suppliers, loading: false
      };
    case supplierConstants.GETALL_FAILURE:
      return {
        ...state, error: "Failed to load Suppliers", loading: false
      };
    case supplierConstants.UPDATE_REQUEST:
      return {
        ...state, loading: true
      };
    case supplierConstants.UPDATE_SUCCESS:
      return {
        ...state, loading: false
      };
    case supplierConstants.UPDATE_FAILURE:
      return {
        ...state, error: "Failed to update Supplier", loading: false
      };
    case supplierConstants.ADD_REQUEST:
      return {
        ...state, loading: true
      };
    case supplierConstants.ADD_SUCCESS:
      return {
        ...state, loading: false
      };
    case supplierConstants.ADD_FAILURE:
      return {
        ...state, error: "Failed to add Supplier", loading: false
      };
    case supplierConstants.REMOVE_REQUEST:
      return {
        ...state, loading: true
      };
    case supplierConstants.REMOVE_SUCCESS:
      return {
        ...state, loading: false
      };
    case supplierConstants.REMOVE_FAILURE:
      return {
        ...state, error: "Failed to delete Supplier", loading: false
      };
    default:
      return state
  }
}