import { customerConstants } from '../_constants';

export function customers(state = {}, action) {
  switch (action.type) {
    case customerConstants.GETFILTEREDDATA_REQUEST:
      return {        
        ...state, loading: true
      };
    case customerConstants.GETFILTEREDDATA_SUCCESS:
      return {
        ...state, customers: action.data, loading: false, totalRecords:  action.totalRecords, pageSize : action.pageSize, pageNumber: action.pageNumber,
        sortField: action.sortField, sortOrder: action.sortOrder
      };
    case customerConstants.GETFILTEREDDATA_FAILURE:
      return {
        ...state, error: "Failed to load Customers grid Data", loading: false
      };
    case customerConstants.GETALL_REQUEST:
      return {        
        ...state, loading: true
      };
    case customerConstants.GETALL_SUCCESS:
      return {
        ...state, customers: action.customers, loading: false
      };
    case customerConstants.GETALL_FAILURE:
      return {
        ...state, error: "Failed to load Customers", loading: false
      };
    case customerConstants.UPDATE_REQUEST:
      return {
        ...state, loading: true
      };
    case customerConstants.UPDATE_SUCCESS:
      return {
        ...state, loading: false
      };
    case customerConstants.UPDATE_FAILURE:
      return {
        ...state, error: "Failed to update Customer", loading: false
      };
    case customerConstants.ADD_REQUEST:
      return {
        ...state, loading: true
      };
    case customerConstants.ADD_SUCCESS:
      return {
        ...state, loading: false
      };
    case customerConstants.ADD_FAILURE:
      return {
        ...state, error: "Failed to add Customer", loading: false
      };
    case customerConstants.REMOVE_REQUEST:
      return {
        ...state, loading: true
      };
    case customerConstants.REMOVE_SUCCESS:
      return {
        ...state, loading: false
      };
    case customerConstants.REMOVE_FAILURE:
      return {
        ...state, error: "Failed to delete Customer", loading: false
      };
    default:
      return state
  }
}