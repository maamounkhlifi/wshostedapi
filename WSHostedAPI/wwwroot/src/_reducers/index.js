import { combineReducers } from 'redux';
import { reducer as burgerMenu } from 'redux-burger-menu';

import { authentication } from './authentication.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { customers } from './customer.reducer';
import { orders } from './order.reducer';
import { products } from './product.reducer';
import { suppliers } from './supplier.reducer';


const rootReducer = combineReducers({
  authentication,
  users,
  alert,
  customers,
  orders,
  products,
  suppliers,
  burgerMenu
});

export default rootReducer;