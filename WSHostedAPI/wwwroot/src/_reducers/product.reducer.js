import { productConstants } from '../_constants';

export function products(state = {}, action) {
  switch (action.type) {
    case productConstants.GETFILTEREDDATA_REQUEST:
      return {        
        ...state, loading: true
      };
    case productConstants.GETFILTEREDDATA_SUCCESS:
      return {
        ...state, products: action.data, loading: false, totalRecords:  action.totalRecords, pageSize : action.pageSize, pageNumber: action.pageNumber,
        sortField: action.sortField, sortOrder: action.sortOrder
      };
    case productConstants.GETFILTEREDDATA_FAILURE:
      return {
        ...state, error: "Failed to load Products grid Data", loading: false
      };
    case productConstants.GETALL_REQUEST:
      return {        
        ...state, loading: true
      };
    case productConstants.GETALL_SUCCESS:
      return {
        ...state, products: action.products, loading: false
      };
    case productConstants.GETALL_FAILURE:
      return {
        ...state, error: "Failed to load Products", loading: false
      };
    case productConstants.UPDATE_REQUEST:
      return {
        ...state, loading: true
      };
    case productConstants.UPDATE_SUCCESS:
      return {
        ...state, loading: false
      };
    case productConstants.UPDATE_FAILURE:
      return {
        ...state, error: "Failed to update Product", loading: false
      };
    case productConstants.ADD_REQUEST:
      return {
        ...state, loading: true
      };
    case productConstants.ADD_SUCCESS:
      return {
        ...state, loading: false
      };
    case productConstants.ADD_FAILURE:
      return {
        ...state, error: "Failed to add Product", loading: false
      };
    case productConstants.REMOVE_REQUEST:
      return {
        ...state, loading: true
      };
    case productConstants.REMOVE_SUCCESS:
      return {
        ...state, loading: false
      };
    case productConstants.REMOVE_FAILURE:
      return {
        ...state, error: "Failed to delete Product", loading: false
      };
    default:
      return state
  }
}