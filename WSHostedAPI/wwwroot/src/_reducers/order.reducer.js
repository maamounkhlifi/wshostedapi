import { orderConstants } from '../_constants';

export function orders(state = {}, action) {
  switch (action.type) {
    case orderConstants.GETFILTEREDDATA_REQUEST:
      return {        
        ...state, loading: true
      };
    case orderConstants.GETFILTEREDDATA_SUCCESS:
      return {
        ...state, orders: action.data, loading: false, totalRecords:  action.totalRecords, pageSize : action.pageSize, pageNumber: action.pageNumber,
        sortField: action.sortField, sortOrder: action.sortOrder
      };
    case orderConstants.GETFILTEREDDATA_FAILURE:
      return {
        ...state, error: "Failed to load Orders grid Data", loading: false
      };
    case orderConstants.GETALL_REQUEST:
      return {        
        ...state, loading: true
      };
    case orderConstants.GETALL_SUCCESS:
      return {
        ...state, orders: action.orders, loading: false
      };
    case orderConstants.GETALL_FAILURE:
      return {
        ...state, error: "Failed to load Orders", loading: false
      };
    case orderConstants.UPDATE_REQUEST:
      return {
        ...state, loading: true
      };
    case orderConstants.UPDATE_SUCCESS:
      return {
        ...state, loading: false
      };
    case orderConstants.UPDATE_FAILURE:
      return {
        ...state, error: "Failed to update Order", loading: false
      };
    case orderConstants.ADD_REQUEST:
      return {
        ...state, loading: true
      };
    case orderConstants.ADD_SUCCESS:
      return {
        ...state, loading: false
      };
    case orderConstants.ADD_FAILURE:
      return {
        ...state, error: "Failed to add Order", loading: false
      };
    case orderConstants.REMOVE_REQUEST:
      return {
        ...state, loading: true
      };
    case orderConstants.REMOVE_SUCCESS:
      return {
        ...state, loading: false
      };
    case orderConstants.REMOVE_FAILURE:
      return {
        ...state, error: "Failed to delete Order", loading: false
      };
    default:
      return state
  }
}