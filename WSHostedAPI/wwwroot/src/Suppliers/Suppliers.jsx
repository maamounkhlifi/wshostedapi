import React from 'react';
import { SuppliersGrid } from '../_grid';

class Suppliers extends React.Component {
    constructor(props) {
        super(props);

    }    

    componentDidMount() {
    }

    render() {
        return (
            <div>
                Suppliers                
                <SuppliersGrid></SuppliersGrid>
            </div>
        );
    }
}

export { Suppliers };