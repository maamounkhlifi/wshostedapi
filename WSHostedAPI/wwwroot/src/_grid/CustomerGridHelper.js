import React from 'react';
import BaseGridHelper from './BaseGridHelper';
import { Column } from 'primereact/column';
import { customerActions }  from '../_actions/customer.actions'; // must be declared
import { CustomerService } from '../_services'; // must be declared

export { CustomerGridHelper };

function CustomerGridHelper() {
    this.actions = customerActions;
    this.service = new CustomerService();
}

CustomerGridHelper.prototype = Object.create(BaseGridHelper.prototype); // Inherit!

CustomerGridHelper.prototype.getNew = function(){
    return  {
        firstName: '',
        lastName: '',
        city: '',
        country: '',
        phone: ''
    };
}

CustomerGridHelper.prototype.getColumns = function () {
    let cols = [
        { field: "firstName", header: "First Name", filter: true, filterElement: this.getAutoCompleteFilter("firstName"), sortable:true },
        { field: "lastName", header: "Last Name", filter: true, filterElement: this.getInputTextFilter("lastName"), sortable:true  },
        { field: "city", header: "City", filter: true, filterElement: this.getInputTextFilter("city"), sortable:true  },
        { field: "country", header: "Country", filter: true, filterElement: this.getInputTextFilter("country"), sortable:true  },
        { field: "phone", header: "Phone", filter: true, filterElement: this.getInputTextFilter("phone"), sortable:true  }

    ];

    return cols.map((col, i) => {
        return <Column key={col.field} field={col.field} header={col.header} filter={col.filter} filterElement={col.filterElement} sortable={col.sortable} />;
    });
}