import React from 'react';
import BaseGridHelper from './BaseGridHelper';
import moment from 'moment';
import { Column } from 'primereact/column';
import { orderActions }  from '../_actions/order.actions'; // must be declared

export { OrderGridHelper };

function OrderGridHelper() {    
    this.actions = orderActions;
}

OrderGridHelper.prototype = Object.create(BaseGridHelper.prototype); // Inherit!

OrderGridHelper.prototype.getNew = function(){
    return  {
        orderDate: null,
        orderNumber: '',
        customerId: null,
        totalAmount: null
    };
}

OrderGridHelper.prototype.orderDateTemplate = function(rowData, column) {
    return <span>{moment(rowData['orderDate']).format('MMMM D YYYY')}</span>;
}

OrderGridHelper.prototype.getColumns = function () {
    let cols = [
        { field: "orderDate", header: "Order Date", body: this.orderDateTemplate, filter: true, filterElement: this.getDateFilter("orderDate"), sortable:true   },
        { field: "orderNumber", header: "Order Number", filter: true, filterElement: this.getNumberFilter("orderNumber"), sortable:true   },
        { field: "customerId", header: "Customer ID", filter: true, filterElement: this.getNumberFilter("customerId"), sortable:true   },
        { field: "totalAmount", header: "Total Amount", filter: true, filterElement: this.getDecimalFilter("totalAmount"), sortable:true   }

    ];

    return cols.map((col, i) => {
        return <Column key={col.field} field={col.field} header={col.header} body={col.body} filter={col.filter} filterElement={col.filterElement} sortable={col.sortable} />;
    });
}