import React from 'react';
import { connect } from 'react-redux';
import { DataTable } from 'primereact/datatable';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';
import { Calendar } from 'primereact/calendar';
import { Spinner } from 'primereact/spinner';
import { OrderGridHelper } from './OrderGridHelper';

class OrdersGrid extends React.Component {
    constructor(props) {
        super(props);
        this.Helper = new OrderGridHelper();
        this.Helper.initHelper("order", this);
        this.state = {
            order: this.Helper.getNew(),
            selectedOrder: this.Helper.getNew(),
            filter: this.Helper.getNew()
        };
        this.save = this.save.bind(this);
        this.onOrderSelect = this.onOrderSelect.bind(this);
        this.delete = this.delete.bind(this);
        this.addNew = this.addNew.bind(this);
        this.onPage = this.onPage.bind(this);
        this.onSort = this.onSort.bind(this);
    }

    save() {
        if (this.newOrder) {
            this.Helper.add(this.state.order);
            this.Helper.getFilteredData({ 
                pageNumber: 0, 
                pageSize: this.props.pageSize, 
                sortField: this.props.sortField, 
                sortOrder: this.props.sortOrder}); //Update Grid
        }
        else{
            this.Helper.update(this.state.order, this.state.order.id);
            this.Helper.getFilteredData({ 
                pageNumber: 0, 
                pageSize: this.props.pageSize, 
                sortField: this.props.sortField, 
                sortOrder: this.props.sortOrder}); //Update Grid
        }
        this.setState({
            selectedOrder: null,
            order: null,
            displayDialog: false
        });
    }

    delete() {
        this.Helper.remove(this.state.selectedOrder.id);
        this.Helper.getFilteredData({ pageNumber: 0, pageSize: this.props.pageSize }); //Update Grid        
        this.setState({
            selectedOrder: null,
            order: null,
            displayDialog: false
        });
    }

    onOrderSelect(e) {
        var order = Object.assign({}, e.data);
        order.orderDate = new Date(order.orderDate);
        this.setState({
            displayDialog: true,
            order: order
        });
        this.newOrder = false;
    }

    addNew() {
        this.newOrder = true;
        this.setState({
            order: this.Helper.getNew(),
            displayDialog: true
        });
    }

    updateProperty(property, value) {
        let order = this.state.order;
        order[property] = value;
        this.setState({ order: order });
    }    

    onSort(event){
        var filters = {
            pageNumber: this.props.pageNumber,
            pageSize: this.props.pageSize,
            sortField: event.sortField, 
            sortOrder: event.sortOrder,
            filter: this.state.filter
        }
        this.Helper.getFilteredData(filters);
    }

    onPage(event) {
        var filters = {
            pageNumber: event.page,
            pageSize: event.rows,
            sortField: this.props.sortField, 
            sortOrder: this.props.sortOrder,
            filter: this.state.filter
        }
        this.Helper.getFilteredData(filters);
    }  

    componentDidMount() {
        this.Helper.getFilteredData({
            pageSize: 10,
            pageNumber: 0,
            sortField: null, 
            sortOrder: 1
        });
    }

    render() {
        const { orders, loading, pageSize, pageNumber, totalRecords, sortField, sortOrder } = this.props;
        var first = pageNumber * pageSize + 1;

        let footer = <div className="p-clearfix" style={{ width: '100%' }}>
            <Button style={{ float: 'left' }} label="Add" icon="pi pi-plus" onClick={this.addNew} />
        </div>;

        let dialogFooter = <div className="ui-dialog-buttonpane p-clearfix">
            {!this.newOrder && <Button label="Delete" icon="pi pi-times" onClick={this.delete} />}
            <Button label="Save" icon="pi pi-check" onClick={this.save} />
        </div>;

        return (
            <div>
                {orders &&
                    <DataTable id="orderGrid" value={orders} paginator={true} rows={pageSize} first={first} rowsPerPageOptions={[5, 10, 20]}
                        selectionMode="single" selection={this.state.selectedOrder} lazy={true} totalRecords={totalRecords}
                        footer={footer} onSelectionChange={e => this.setState({ selectedOrder: e.value })}
                        onRowSelect={this.onOrderSelect} onPage={this.onPage} loading={loading}
                        sortField={sortField} sortOrder={sortOrder} onSort={this.onSort}>
                        {this.Helper.getColumns()}
                    </DataTable>
                }

                <Dialog visible={this.state.displayDialog} style={{ width: '50vw' }} header="Order Details" modal={true} footer={dialogFooter} onHide={() => this.setState({ displayDialog: false })}>
                    {
                        this.state.order &&

                        <div className="p-grid p-fluid">
                            <div className="p-col-4" style={{ padding: '.75em' }}><label htmlFor="orderDate">Order Date</label></div>
                            <div className="p-col-8" style={{ padding: '.5em' }}>
                                <Calendar id="orderDate" onChange={(e) => { this.updateProperty('orderDate', e.target.value) }} value={this.state.order.orderDate}
                                    dateFormat="MM d yy" />
                            </div>

                            <div className="p-col-4" style={{ padding: '.75em' }}><label htmlFor="orderNumber">Order Number</label></div>
                            <div className="p-col-8" style={{ padding: '.5em' }}>
                                <InputText id="orderNumber" onChange={(e) => { this.updateProperty('orderNumber', e.target.value) }} value={this.state.order.orderNumber} />
                            </div>

                            <div className="p-col-4" style={{ padding: '.75em' }}><label htmlFor="customerId">Customer ID</label></div>
                            <div className="p-col-8" style={{ padding: '.5em' }}>
                                <Spinner id="customerId" min={1} onChange={(e) => { this.updateProperty('customerId', e.target.value) }} value={this.state.order.customerId} />
                            </div>

                            <div className="p-col-4" style={{ padding: '.75em' }}><label htmlFor="totalAmount">Total Amount</label></div>
                            <div className="p-col-8" style={{ padding: '.5em' }}>
                                <Spinner id="totalAmount" min={0} step={0.01} onChange={(e) => { this.updateProperty('totalAmount', e.target.value) }} value={this.state.order.totalAmount} />
                            </div>

                        </div>
                    }
                </Dialog>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { orders } = state;
    return {
        orders: orders.orders,
        response: orders.response,
        loading: orders.loading,
        pageSize: orders.pageSize,
        pageNumber: orders.pageNumber,
        sortOrder: orders.sortOrder,
        sortField: orders.sortField,
        totalRecords: orders.totalRecords
    };
}

const connectedOrdersGrid = connect(mapStateToProps)(OrdersGrid);
export { connectedOrdersGrid as OrdersGrid };