import React from 'react';
import { store } from '../_helpers/store';
import { InputText } from 'primereact/inputtext';
import { AutoComplete } from 'primereact/autocomplete';
import { TriStateCheckbox } from 'primereact/triStateCheckbox';
import { Spinner } from 'primereact/spinner';
import { Calendar } from 'primereact/calendar';
//import { entityActions as actions }  from '../_actions/entityActions'; // must be declared

export default function BaseGridHelper() {
    //this.actions = entityActions;
}

BaseGridHelper.prototype = {};

BaseGridHelper.prototype.initHelper = function (controller, grid) {
    this.controller = controller;
    this.grid = grid;    
};

BaseGridHelper.prototype.getNew = function () {
    // TO OVERRIDE
}

BaseGridHelper.prototype.getColumns = function () {
    // TO OVERRIDE
}

BaseGridHelper.prototype.getAll = function () {
    store.dispatch(this.actions.getAll());
}

BaseGridHelper.prototype.getFilteredData = function (filters) {
    store.dispatch(this.actions.getFilteredData(filters));
}

BaseGridHelper.prototype.add = function (model) {
    store.dispatch(this.actions.add(model));
}

BaseGridHelper.prototype.update = function (model, id) {
    store.dispatch(this.actions.update(model, id));
}

BaseGridHelper.prototype.remove = function (id) {
    store.dispatch(this.actions.remove(id));
}

BaseGridHelper.prototype.getInputTextFilter = function (field) {
    let self = this;
    return <InputText style={{ width: '100%' }} value={self.grid.state.filter[field]}
        onChange={(event) => {
            self.grid.setState({ filter: { ...self.grid.state.filter, [field]: event.target.value } }, function () {
                if (self.filterTimeOut) {
                    clearTimeout(self.filterTimeOut)
                }
                self.filterTimeOut = setTimeout(function () {
                    self.getFilteredData({
                        pageNumber: self.grid.props.pageNumber, pageSize: self.grid.props.pageSize, filter: self.grid.state.filter
                    })
                }, 1000)
            });

        }} />
}

BaseGridHelper.prototype.getAutoCompleteFilter = function (field) {
    let self = this;
    return <AutoComplete style={{ width: '100%' }} value={self.grid.state.filter[field]} suggestions={self.grid.state.filter[field + 'Suggestions']}
        onChange={(event) => {
            self.grid.setState({ filter: { ...self.grid.state.filter, [field]: event.value }});
        }}
        onKeyUp={(event) => {
            if(event.keyCode === 13){
                self.grid.setState({ filter: { ...self.grid.state.filter, [field]: event.currentTarget.value } }, function () {
                    if (self.filterTimeOut) {
                        clearTimeout(self.filterTimeOut)
                    }
                    self.filterTimeOut = setTimeout(function () {
                        self.getFilteredData({
                            pageNumber: self.grid.props.pageNumber, pageSize: self.grid.props.pageSize, filter: self.grid.state.filter
                        })
                    }, 1000)
                });
            }
        }}
        onSelect={(event) => {
            self.grid.setState({ filter: { ...self.grid.state.filter, [field]: event.value } }, function () {                
                self.getFilteredData({
                    pageNumber: self.grid.props.pageNumber, pageSize: self.grid.props.pageSize, filter: self.grid.state.filter
                })     
            });

        }} 
        completeMethod={(event) => {            
            this.service.getAllRecordsForField(event.query, field).then(
                result => {                     
                    self.grid.setState({
                        filter: { ...self.grid.state.filter, [field + 'Suggestions']: result }
                    })
                },
                error => {
                    alert("error " + error);
                    self.grid.setState({
                        filter: { ...self.grid.state.filter, [field + 'Suggestions']: [] }
                    })
                }
            )                
        }}/>
}



BaseGridHelper.prototype.getNumberFilter = function (field) {
    let self = this;
    return <Spinner style={{ width: '100%' }} step={1} value={self.grid.state.filter[field]}
        onChange={(event) => {
            self.grid.setState({ filter: { ...self.grid.state.filter, [field]: event.target.value } }, function () {
                if (self.filterTimeOut) {
                    clearTimeout(self.filterTimeOut)
                }
                self.filterTimeOut = setTimeout(function () {
                    self.getFilteredData({
                        pageNumber: self.grid.props.pageNumber, pageSize: self.grid.props.pageSize, filter: self.grid.state.filter
                    })
                }, 1000)
            });

        }} />
}

BaseGridHelper.prototype.getDecimalFilter = function (field) {
    let self = this;
    return <Spinner style={{ width: '100%' }} step={0.1} value={self.grid.state.filter[field]}
        onChange={(event) => {
            self.grid.setState({ filter: { ...self.grid.state.filter, [field]: event.target.value } }, function () {
                if (self.filterTimeOut) {
                    clearTimeout(self.filterTimeOut)
                }
                self.filterTimeOut = setTimeout(function () {
                    self.getFilteredData({
                        pageNumber: self.grid.props.pageNumber, pageSize: self.grid.props.pageSize, filter: self.grid.state.filter
                    })
                }, 1000)
            });

        }} />
}


BaseGridHelper.prototype.getCheckBoxFilter = function (field) {
    let self = this;
    return <TriStateCheckbox style={{ width: '100%' }} value={self.grid.state.filter[field]}
        onChange={(event) => {
            self.grid.setState({ filter: { ...self.grid.state.filter, [field]: event.value } }, function () {
                if (self.filterTimeOut) {
                    clearTimeout(self.filterTimeOut)
                }
                self.filterTimeOut = setTimeout(function () {
                    self.getFilteredData({
                        pageNumber: self.grid.props.pageNumber, pageSize: self.grid.props.pageSize, filter: self.grid.state.filter
                    })
                }, 1000)
            });

        }} />
}

BaseGridHelper.prototype.getDateFilter = function (field) {
    let self = this;
    return <Calendar monthNavigator={true} yearNavigator={true}  style={{ width: '100%' }} value={self.grid.state.filter[field]} dateFormat="MM d yy" yearRange="1900:2030"
        onChange={(event) => {
            self.grid.setState({ filter: { ...self.grid.state.filter, [field]: event.target.value } }, function () {
                if (self.filterTimeOut) {
                    clearTimeout(self.filterTimeOut)
                }
                self.filterTimeOut = setTimeout(function () {
                    self.getFilteredData({
                        pageNumber: self.grid.props.pageNumber, pageSize: self.grid.props.pageSize, filter: self.grid.state.filter
                    })
                }, 1000)
            });

        }} />
}