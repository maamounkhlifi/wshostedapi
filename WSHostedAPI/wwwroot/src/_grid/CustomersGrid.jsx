import React from 'react';
import { connect } from 'react-redux';
import { DataTable } from 'primereact/datatable';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';
import { CustomerGridHelper } from './CustomerGridHelper';

class CustomersGrid extends React.Component {
    constructor(props) {
        super(props);
        this.Helper = new CustomerGridHelper();
        this.Helper.initHelper("customer", this);
        this.state = {
            customer: this.Helper.getNew(),
            selectedCustomer: this.Helper.getNew(),
            filter: {...this.Helper.getNew(), firstNameSuggestions: []}
        };
        this.save = this.save.bind(this);
        this.onCustomerSelect = this.onCustomerSelect.bind(this);
        this.delete = this.delete.bind(this);
        this.addNew = this.addNew.bind(this);
        this.onPage = this.onPage.bind(this);
        this.onSort = this.onSort.bind(this);
    }

    save() {
        if (this.newCustomer) {
            this.Helper.add(this.state.customer);
            this.Helper.getFilteredData({ 
                pageNumber: 0, 
                pageSize: this.props.pageSize, 
                sortField: this.props.sortField, 
                sortOrder: this.props.sortOrder}); //Update Grid        
            }
        else {
            this.Helper.update(this.state.customer, this.state.customer.id);
            this.Helper.getFilteredData({ 
                pageNumber: 0, 
                pageSize: this.props.pageSize, 
                sortField: this.props.sortField, 
                sortOrder: this.props.sortOrder}); //Update Grid        
            }
        this.setState({
            selectedCustomer: null,
            customer: null,
            displayDialog: false
        });
    }

    delete() {
        this.Helper.remove(this.state.selectedCustomer.id);
        this.Helper.getFilteredData({ pageNumber: 0, pageSize: this.props.pageSize }); //Update Grid
        this.setState({
            selectedCustomer: null,
            customer: null,
            displayDialog: false
        });
    }

    onCustomerSelect(e) {
        this.setState({
            displayDialog: true,
            customer: Object.assign({}, e.data)
        });
        this.newCustomer = false;
        
    }

    addNew() {
        this.newCustomer = true;
        this.setState({
            customer: this.Helper.getNew(),
            displayDialog: true
        });
    }

    updateProperty(property, value) {
        let customer = this.state.customer;
        customer[property] = value;
        this.setState({ customer: customer });
    }

    onSort(event){
        var filters = {
            pageNumber: this.props.pageNumber,
            pageSize: this.props.pageSize,
            sortField: event.sortField, 
            sortOrder: event.sortOrder,
            filter: this.state.filter
        }
        this.Helper.getFilteredData(filters);
    }

    onPage(event) {
        var filters = {
            pageNumber: event.page,
            pageSize: event.rows,
            sortField: this.props.sortField, 
            sortOrder: this.props.sortOrder,
            filter: this.state.filter
        }
        this.Helper.getFilteredData(filters);
    }

    componentDidMount() {
        this.Helper.getFilteredData({
            pageSize: 10,
            pageNumber: 0,
            sortField: null, 
            sortOrder: 1
        });
    }

    render() {
        const { customers, loading, pageSize, pageNumber, totalRecords, sortField, sortOrder } = this.props;
        var first = pageNumber * pageSize + 1;
        let footer = <div className="p-clearfix" style={{ width: '100%' }}>
            <Button style={{ float: 'left' }} label="Add" icon="pi pi-plus" onClick={this.addNew} />
        </div>;

        let dialogFooter = <div className="ui-dialog-buttonpane p-clearfix">
            {!this.newCustomer && <Button label="Delete" icon="pi pi-times" onClick={this.delete} />}
            <Button label="Save" icon="pi pi-check" onClick={this.save} />
        </div>;        

        return (
            <div>
                {customers &&
                    <DataTable id="customerGrid" value={customers} paginator={true} rows={pageSize} first={first} rowsPerPageOptions={[5, 10, 20]}
                        selectionMode="single" selection={this.state.selectedCustomer} lazy={true} totalRecords={totalRecords}
                        footer={footer} onSelectionChange={e => this.setState({ selectedCustomer: e.value })}
                        onRowSelect={this.onCustomerSelect} onPage={this.onPage} loading={loading}
                        sortField={sortField} sortOrder={sortOrder} onSort={this.onSort}>
                        {this.Helper.getColumns()}
                    </DataTable>
                }

                <Dialog visible={this.state.displayDialog} style={{ width: '50vw' }} header="Customer Details" modal={true} footer={dialogFooter} onHide={() => this.setState({ displayDialog: false })}>
                    {
                        this.state.customer &&

                        <div className="p-grid p-fluid">
                            <div className="p-col-4" style={{ padding: '.75em' }}><label htmlFor="firstName">First Name</label></div>
                            <div className="p-col-8" style={{ padding: '.5em' }}>
                                <InputText id="firstName" onChange={(e) => { this.updateProperty('firstName', e.target.value) }} value={this.state.customer.firstName} />
                            </div>

                            <div className="p-col-4" style={{ padding: '.75em' }}><label htmlFor="lastName">Last Name	</label></div>
                            <div className="p-col-8" style={{ padding: '.5em' }}>
                                <InputText id="lastName" onChange={(e) => { this.updateProperty('lastName', e.target.value) }} value={this.state.customer.lastName} />
                            </div>

                            <div className="p-col-4" style={{ padding: '.75em' }}><label htmlFor="city">City</label></div>
                            <div className="p-col-8" style={{ padding: '.5em' }}>
                                <InputText id="city" onChange={(e) => { this.updateProperty('city', e.target.value) }} value={this.state.customer.city} />
                            </div>

                            <div className="p-col-4" style={{ padding: '.75em' }}><label htmlFor="country">Country</label></div>
                            <div className="p-col-8" style={{ padding: '.5em' }}>
                                <InputText id="country" onChange={(e) => { this.updateProperty('country', e.target.value) }} value={this.state.customer.country} />
                            </div>

                            <div className="p-col-4" style={{ padding: '.75em' }}><label htmlFor="phone">Phone</label></div>
                            <div className="p-col-8" style={{ padding: '.5em' }}>
                                <InputText id="phone" onChange={(e) => { this.updateProperty('phone', e.target.value) }} value={this.state.customer.phone} />
                            </div>

                        </div>
                    }
                </Dialog>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { customers } = state;
    return {
        customers: customers.customers,
        response: customers.response,
        loading: customers.loading,
        pageSize: customers.pageSize,
        pageNumber: customers.pageNumber,
        sortOrder: customers.sortOrder,
        sortField: customers.sortField,
        totalRecords: customers.totalRecords
    };
}

const connectedCustomersGrid = connect(mapStateToProps)(CustomersGrid);
export { connectedCustomersGrid as CustomersGrid };