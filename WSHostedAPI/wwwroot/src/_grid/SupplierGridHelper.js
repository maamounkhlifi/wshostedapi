import React from 'react';
import BaseGridHelper from './BaseGridHelper';
import { Column } from 'primereact/column';
import { supplierActions }  from '../_actions/supplier.actions'; // must be declared

export { SupplierGridHelper };

function SupplierGridHelper() {    
    this.actions = supplierActions;
}

SupplierGridHelper.prototype = Object.create(BaseGridHelper.prototype); // Inherit!

SupplierGridHelper.prototype.getNew = function(){
    return  {
        companyName: '',
        contactName: '',
        contactTitle: '',
        city: '',
        country: '',
        phone: '',
        fax: ''
    };
}

SupplierGridHelper.prototype.getColumns = function () {
    let cols = [
        { field: "companyName", header: "Company Name", filter: true, filterElement: this.getInputTextFilter("companyName"), sortable:true   },
        { field: "contactName", header: "Contact Name", filter: true, filterElement: this.getInputTextFilter("contactName"), sortable:true   },
        { field: "contactTitle", header: "Contact Title", filter: true, filterElement: this.getInputTextFilter("contactTitle"), sortable:true   },
        { field: "city", header: "City", filter: true, filterElement: this.getInputTextFilter("city"), sortable:true   },
        { field: "country", header: "Country", filter: true, filterElement: this.getInputTextFilter("country"), sortable:true   },
        { field: "phone", header: "Phone", filter: true, filterElement: this.getInputTextFilter("phone"), sortable:true   },
        { field: "fax", header: "Fax", filter: true, filterElement: this.getInputTextFilter("fax"), sortable:true   },

    ];

    return cols.map((col, i) => {
        return <Column key={col.field} field={col.field} header={col.header} filter={col.filter} filterElement={col.filterElement} sortable={col.sortable} />;
    });
}