import React from 'react';
import { connect } from 'react-redux';
import { DataTable } from 'primereact/datatable';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';
import { SupplierGridHelper } from './SupplierGridHelper';

class SuppliersGrid extends React.Component {
    constructor(props) {
        super(props);
        this.Helper = new SupplierGridHelper();
        this.Helper.initHelper("supplier", this);
        this.state = {
            supplier: this.Helper.getNew(),
            selectedSupplier: this.Helper.getNew(),
            filter: this.Helper.getNew()          
        };
        this.save = this.save.bind(this);
        this.onSupplierSelect = this.onSupplierSelect.bind(this);
        this.delete = this.delete.bind(this);
        this.addNew = this.addNew.bind(this);
        this.onPage = this.onPage.bind(this);
        this.onSort = this.onSort.bind(this);
    }

    save() {
        if (this.newSupplier) {
            this.Helper.add(this.state.supplier);
            this.Helper.getFilteredData({ 
                pageNumber: 0, 
                pageSize: this.props.pageSize, 
                sortField: this.props.sortField, 
                sortOrder: this.props.sortOrder}); //Update Grid
        }
        else {
            this.Helper.update(this.state.supplier, this.state.supplier.id);
            this.Helper.getFilteredData({ 
                pageNumber: 0, 
                pageSize: this.props.pageSize,
                sortField: this.props.sortField, 
                sortOrder: this.props.sortOrder }); //Update Grid
        }
        this.setState({
            selectedSupplier: null,
            supplier: null,
            displayDialog: false
        });
    }

    delete() {
        this.remove(this.state.selectedSupplier.id);
        this.setState({
            selectedSupplier: null,
            supplier: null,
            displayDialog: false
        });
    }

    onSupplierSelect(e) {
        this.setState({
            displayDialog: true,
            supplier: Object.assign({}, e.data)
        });
        this.newSupplier = false;

    }

    addNew() {
        this.newSupplier = true;
        this.setState({
            supplier: this.Helper.getNew(),
            displayDialog: true
        });
    }

    updateProperty(property, value) {
        let supplier = this.state.supplier;
        supplier[property] = value;
        this.setState({ supplier: supplier });
    }

    onSort(event){
        var filters = {
            pageNumber: this.props.pageNumber,
            pageSize: this.props.pageSize,
            sortField: event.sortField, 
            sortOrder: event.sortOrder,
            filter: this.state.filter
        }
        this.Helper.getFilteredData(filters);
    }

    onPage(event) {
        var filters = {
            pageNumber: event.page,
            pageSize: event.rows,
            sortField: this.props.sortField, 
            sortOrder: this.props.sortOrder,
            filter: this.state.filter
        }
        this.Helper.getFilteredData(filters);
    }

    componentDidMount() {
        this.Helper.getFilteredData({
            pageSize: 10,
            pageNumber: 0,
            sortField: null, 
            sortOrder: 1
        });
    }

    render() {
        const { suppliers, loading, pageSize, pageNumber, totalRecords, sortField, sortOrder } = this.props;
        var first = pageNumber * pageSize + 1;

        let footer = <div className="p-clearfix" style={{ width: '100%' }}>
            <Button style={{ float: 'left' }} label="Add" icon="pi pi-plus" onClick={this.addNew} />
        </div>;

        let dialogFooter = <div className="ui-dialog-buttonpane p-clearfix">
            {!this.newSupplier && <Button label="Delete" icon="pi pi-times" onClick={this.delete} />}
            <Button label="Save" icon="pi pi-check" onClick={this.save} />
        </div>;

        return (
            <div>
                {suppliers &&
                    <DataTable id="supplierGrid" value={suppliers} paginator={true} rows={pageSize} first={first} rowsPerPageOptions={[5, 10, 20]}
                        selectionMode="single" selection={this.state.selectedOrder} lazy={true} totalRecords={totalRecords}
                        footer={footer} onSelectionChange={e => this.setState({ selectedOrder: e.value })}
                        onRowSelect={this.onSupplierSelect} onPage={this.onPage} loading={loading} 
                        sortField={sortField} sortOrder={sortOrder} onSort={this.onSort}>
                        {this.Helper.getColumns()}
                    </DataTable>
                }

                <Dialog visible={this.state.displayDialog} style={{ width: '50vw' }} header="Supplier Details" modal={true} footer={dialogFooter} onHide={() => this.setState({ displayDialog: false })}>
                    {
                        this.state.supplier &&

                        <div className="p-grid p-fluid">
                            <div className="p-col-4" style={{ padding: '.2em' }}><label htmlFor="companyName">Company Name</label></div>
                            <div className="p-col-8" style={{ padding: '.2em' }}>
                                <InputText id="companyName" onChange={(e) => { this.updateProperty('companyName', e.target.value) }} value={this.state.supplier.companyName} />
                            </div>

                            <div className="p-col-4" style={{ padding: '.2em' }}><label htmlFor="contactName">Contact Name</label></div>
                            <div className="p-col-8" style={{ padding: '.2em' }}>
                                <InputText id="contactName" onChange={(e) => { this.updateProperty('contactName', e.target.value) }} value={this.state.supplier.contactName} />
                            </div>

                            <div className="p-col-4" style={{ padding: '.2em' }}><label htmlFor="contactTitle">Contact Title</label></div>
                            <div className="p-col-8" style={{ padding: '.2em' }}>
                                <InputText id="contactTitle" onChange={(e) => { this.updateProperty('contactTitle', e.target.value) }} value={this.state.supplier.contactTitle || ''} />
                            </div>

                            <div className="p-col-4" style={{ padding: '.2em' }}><label htmlFor="city">City</label></div>
                            <div className="p-col-8" style={{ padding: '.2em' }}>
                                <InputText id="city" onChange={(e) => { this.updateProperty('city', e.target.value) }} value={this.state.supplier.city} />
                            </div>

                            <div className="p-col-4" style={{ padding: '.2em' }}><label htmlFor="country">Country</label></div>
                            <div className="p-col-8" style={{ padding: '.2em' }}>
                                <InputText id="country" onChange={(e) => { this.updateProperty('country', e.target.value) }} value={this.state.supplier.country} />
                            </div>

                            <div className="p-col-4" style={{ padding: '.2em' }}><label htmlFor="phone">Phone</label></div>
                            <div className="p-col-8" style={{ padding: '.2em' }}>
                                <InputText id="phone" onChange={(e) => { this.updateProperty('phone', e.target.value) }} value={this.state.supplier.phone} />
                            </div>

                            <div className="p-col-4" style={{ padding: '.2em' }}><label htmlFor="fax">Fax</label></div>
                            <div className="p-col-8" style={{ padding: '.2em' }}>
                                <InputText id="fax" onChange={(e) => { this.updateProperty('fax', e.target.value) }} value={this.state.supplier.fax || ''} />
                            </div>

                        </div>
                    }
                </Dialog>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { suppliers } = state;
    return {
        suppliers: suppliers.suppliers,
        response: suppliers.response,
        loading: suppliers.loading,
        pageSize: suppliers.pageSize,
        pageNumber: suppliers.pageNumber,
        sortOrder: suppliers.sortOrder,
        sortField: suppliers.sortField,
        totalRecords: suppliers.totalRecords
    };
}

const connectedSuppliersGrid = connect(mapStateToProps)(SuppliersGrid);
export { connectedSuppliersGrid as SuppliersGrid };