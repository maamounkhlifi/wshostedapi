import React from 'react';
import { connect } from 'react-redux';
import { DataTable } from 'primereact/datatable';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';
import { Checkbox } from 'primereact/checkbox';
import { Spinner } from 'primereact/spinner';
import { ProductGridHelper } from './ProductGridHelper';

class ProductsGrid extends React.Component {
    constructor(props) {
        super(props);
        this.Helper = new ProductGridHelper();
        this.Helper.initHelper("product", this);
        this.state = {
            product: this.Helper.getNew(),
            selectedProduct: this.Helper.getNew(),
            filter: this.Helper.getNew()            
        };
        this.save = this.save.bind(this);
        this.onProductSelect = this.onProductSelect.bind(this);
        this.delete = this.delete.bind(this);
        this.addNew = this.addNew.bind(this);
        this.onPage = this.onPage.bind(this);
        this.onSort = this.onSort.bind(this);
    }

    save() {
        if (this.newProduct) {
            this.Helper.add(this.state.product);
            this.Helper.getFilteredData({ 
                pageNumber: 0, 
                pageSize: this.props.pageSize, 
                sortField: this.props.sortField, 
                sortOrder: this.props.sortOrder}); //Update Grid
        }
        else {
            this.Helper.update(this.state.product, this.state.product.id);
            this.Helper.getFilteredData({ 
                pageNumber: 0, 
                pageSize: this.props.pageSize, 
                sortField: this.props.sortField, 
                sortOrder: this.props.sortOrder}); //Update Grid
        }
        this.setState({
            selectedProduct: null,
            product: null,
            displayDialog: false
        });
    }

    delete() {
        this.Helper.remove(this.state.selectedProduct.id);
        this.Helper.getFilteredData({ pageNumber: 0, pageSize: this.props.pageSize }); //Update Grid        
        this.setState({
            selectedProduct: null,
            product: null,
            displayDialog: false
        });
    }

    onProductSelect(e) {
        this.setState({
            displayDialog: true,
            product: Object.assign({}, e.data)
        });
        this.newProduct = false;

    }

    addNew() {
        this.newProduct = true;
        this.setState({
            product: this.Helper.getNew(),
            displayDialog: true
        });
    }

    updateProperty(property, value) {
        let product = this.state.product;
        product[property] = value;
        this.setState({ product: product });
    }

    onSort(event){
        var filters = {
            pageNumber: this.props.pageNumber,
            pageSize: this.props.pageSize,
            sortField: event.sortField, 
            sortOrder: event.sortOrder,
            filter: this.state.filter
        }
        this.Helper.getFilteredData(filters);
    }

    onPage(event) {
        var filters = {
            pageNumber: event.page,
            pageSize: event.rows,
            sortField: this.props.sortField, 
            sortOrder: this.props.sortOrder,
            filter: this.state.filter
        }
        this.Helper.getFilteredData(filters);
    }

    componentDidMount() {
        this.Helper.getFilteredData({
            pageSize: 10,
            pageNumber: 0,
            sortField: null, 
            sortOrder: 1
        });
    }

    render() {
        const { products, loading, pageSize, pageNumber, totalRecords, sortField, sortOrder } = this.props;
        var first = pageNumber * pageSize + 1;

        let footer = <div className="p-clearfix" style={{ width: '100%' }}>
            <Button style={{ float: 'left' }} label="Add" icon="pi pi-plus" onClick={this.addNew} />
        </div>;

        let dialogFooter = <div className="ui-dialog-buttonpane p-clearfix">
            {!this.newProduct && <Button label="Delete" icon="pi pi-times" onClick={this.delete} />}
            <Button label="Save" icon="pi pi-check" onClick={this.save} />
        </div>;

        return (
            <div>
                {products &&
                    <DataTable id="productGrid" value={products} paginator={true} rows={pageSize} first={first} rowsPerPageOptions={[5, 10, 20]}
                        selectionMode="single" selection={this.state.selectedProduct} lazy={true} totalRecords={totalRecords}
                        footer={footer} onSelectionChange={e => this.setState({ selectedProduct: e.value })}
                        onRowSelect={this.onProductSelect} onPage={this.onPage} loading={loading}
                        sortField={sortField} sortOrder={sortOrder} onSort={this.onSort}>
                        {this.Helper.getColumns()}
                    </DataTable>
                }

                <Dialog visible={this.state.displayDialog} style={{ width: '50vw' }} header="Product Details" modal={true} footer={dialogFooter} onHide={() => this.setState({ displayDialog: false })}>
                    {
                        this.state.product &&

                        <div className="p-grid p-fluid">
                            <div className="p-col-4" style={{ padding: '.75em' }}><label htmlFor="productName">Product Name</label></div>
                            <div className="p-col-8" style={{ padding: '.5em' }}>
                                <InputText id="productName" onChange={(e) => { this.updateProperty('productName', e.target.value) }} value={this.state.product.productName} />
                            </div>

                            <div className="p-col-4" style={{ padding: '.75em' }}><label htmlFor="supplierId">Supplier ID</label></div>
                            <div className="p-col-8" style={{ padding: '.5em' }}>
                                <Spinner id="supplierId" min={1} onChange={(e) => { this.updateProperty('supplierId', e.target.value) }} value={this.state.product.supplierId} />
                            </div>

                            <div className="p-col-4" style={{ padding: '.75em' }}><label htmlFor="unitPrice">Unit Price</label></div>
                            <div className="p-col-8" style={{ padding: '.5em' }}>
                                <Spinner id="unitPrice" min={0} step={0.1} onChange={(e) => { this.updateProperty('unitPrice', e.target.value) }} value={this.state.product.unitPrice} />
                            </div>

                            <div className="p-col-4" style={{ padding: '.75em' }}><label htmlFor="package">Package</label></div>
                            <div className="p-col-8" style={{ padding: '.5em' }}>
                                <InputText id="package" onChange={(e) => { this.updateProperty('package', e.target.value) }} value={this.state.product.package} />
                            </div>

                            <div className="p-col-4" style={{ padding: '.75em' }}><label htmlFor="isDiscontinued">Is Discontinued</label></div>
                            <div className="p-col-8" style={{ padding: '.5em' }}>
                                <Checkbox id="isDiscontinued" onChange={(e) => { this.updateProperty('isDiscontinued', e.checked) }} checked={this.state.product.isDiscontinued} />
                            </div>

                        </div>
                    }
                </Dialog>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { products } = state;
    return {
        products: products.products,
        response: products.response,
        loading: products.loading,
        pageSize: products.pageSize,
        pageNumber: products.pageNumber,
        sortOrder: products.sortOrder,
        sortField: products.sortField,
        totalRecords: products.totalRecords
    };
}

const connectedProductsGrid = connect(mapStateToProps)(ProductsGrid);
export { connectedProductsGrid as ProductsGrid };