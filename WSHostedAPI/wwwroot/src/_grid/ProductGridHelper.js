import React from 'react';
import BaseGridHelper from './BaseGridHelper';
import { Column } from 'primereact/column';
import { Checkbox } from 'primereact/checkbox';
import { productActions }  from '../_actions/product.actions'; // must be declared

export { ProductGridHelper };

function ProductGridHelper() {    
    this.actions = productActions;
}

ProductGridHelper.prototype = Object.create(BaseGridHelper.prototype); // Inherit!

ProductGridHelper.prototype.getNew = function(){
    return  {
        productName: '',
        supplierId: null,
        unitPrice: null,
        package: '',
        isDiscontinued: false
    };
}

ProductGridHelper.prototype.isDiscontinuedTemplate = function (rowData, column) {
    return <div style={{ textAlign: 'center' }}><Checkbox disabled={true} checked={rowData['isDiscontinued']}></Checkbox></div>;
}

ProductGridHelper.prototype.getColumns = function () {
    let cols = [
        { field: "productName", header: "Product Name", filter: true, filterElement: this.getInputTextFilter("productName"), sortable:true  },
        { field: "supplierId", header: "Supplier ID", filter: true, filterElement: this.getNumberFilter("supplierId"), sortable:true  },
        { field: "unitPrice", header: "Unit Price", filter: true, filterElement: this.getDecimalFilter("unitPrice"), sortable:true  },
        { field: "package", header: "Package", filter: true, filterElement: this.getInputTextFilter("package"), sortable:true  },
        { field: "isDiscontinued", header: "Is Discontinued", body: this.isDiscontinuedTemplate, filter: true, filterElement: this.getCheckBoxFilter("isDiscontinued"), sortable:true },

    ];

    return cols.map((col, i) => {
        return <Column key={col.field} field={col.field} header={col.header} body={col.body} filter={col.filter} filterElement={col.filterElement} sortable={col.sortable} />;
    });
}