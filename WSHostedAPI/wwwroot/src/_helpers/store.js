import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from '../_reducers';

const loggerMiddleware = createLogger();
const initialState = {
    customers: { customers: [], loading: false, pageSize : 10, pageNumber: 0, totalRecords: 0 },
    orders: { orders: [], loading: false, pageSize : 10, pageNumber: 0, totalRecords: 0},
    products: { products: [], loading: false, pageSize : 10, pageNumber: 0, totalRecords: 0},
    suppliers: { suppliers: [], loading: false, pageSize : 10, pageNumber: 0, totalRecords: 0},
}

export const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(
        thunkMiddleware,
        loggerMiddleware
    )
);