import { authenticationService } from '@/_services';

export function authHeader() {
    // return authorization header with jwt token
    const token = authenticationService.getAccessToken();
    if (token)
        return { Authorization: `Bearer ${token}` };
    else
        return {};

}