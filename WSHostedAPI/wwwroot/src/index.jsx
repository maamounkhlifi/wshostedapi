import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import { store } from './_helpers';
import { App } from './App';

import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css'
import 'react-block-ui/style.css';
import './_css/style.css';

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app')
);
if (module.hot) {
    module.hot.accept();
}