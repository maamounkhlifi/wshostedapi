import BaseService from './BaseService';
export { OrderItemService };

function OrderItemService() {
    this.controller = "OrderItems";
}

OrderItemService.prototype = Object.create(BaseService.prototype); // Inherit!

