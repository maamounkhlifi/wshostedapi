import BaseService from './BaseService';
export { OrderService };

function OrderService() {
    this.controller = "Orders";
}

OrderService.prototype = Object.create(BaseService.prototype); // Inherit!

