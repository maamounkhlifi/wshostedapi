// auth0 authentification dependencies
import auth0 from 'auth0-js';

import config from 'config';
import { handleResponse } from '@/_helpers';
import { store } from '../_helpers/store';
import { userActions } from '../_actions';



// auth0 authentification variables
const auth0Client = new auth0.WebAuth({
    domain: config.domain,
    clientID: config.clientID,
    redirectUri: config.redirectUri,
    audience: config.audience,
    responseType: 'token id_token',
    scope: 'openid profile'
});


function login(username, password) {
    if (localStorage.getItem("authScheme") === "custom") {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ username, password })
        };

        return fetch(`${config.apiUrl}/Users/authenticate`, requestOptions)
            .then(handleResponse)
            .then(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                sessionStorage.setItem('user', JSON.stringify(user));
                return user;
            });
    }
    else
        auth0Client.authorize();

}

function logout(history) {
    let isAuth0 = localStorage.getItem("authScheme") === "auth0";
    // remove user from local storage to log user out
    localStorage.removeItem('user');
    localStorage.removeItem('authScheme');
    if (isAuth0) {
        localStorage.removeItem('access_token');
        localStorage.removeItem('id_token');
        localStorage.removeItem('expires_at');
        auth0Client.logout({
            clientID: config.clientID,
            returnTo: "http://localhost:56226/"
        });
    }
    else
        history.push('/Home/login');

}

function handleAuthentication(history) {
    auth0Client.parseHash((err, authResult) => {        
        if (authResult && authResult.accessToken && authResult.idToken) {
            this.setSession(authResult);
            store.dispatch(userActions.loginAuth0());
        } else if (err) {
            store.dispatch(userActions.loginAuth0(err));
        }
    });
}

function setSession(authResult) {
    let expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    sessionStorage.setItem('expires_at', expiresAt);
    let user = {
        firstName: authResult.idTokenPayload.given_name,
        lastName: authResult.idTokenPayload.family_name
    };
    sessionStorage.setItem('user', JSON.stringify(user));
}

function isAuthenticated() {
    if (localStorage.getItem("authScheme") === "custom")
        return true && sessionStorage.getItem('user');
    else {
        let expiresAt = JSON.parse(sessionStorage.getItem('expires_at'));
        return new Date().getTime() < expiresAt;
    }

}

function getAccessToken() {
    let accessToken;
    if (localStorage.getItem("authScheme") === "custom") {
        accessToken = JSON.parse(sessionStorage.getItem('user')).token;
    }
    else {
        accessToken = localStorage.getItem('access_token');
    }
    if (!accessToken) {
        throw new Error('No access token found');
    }
    return accessToken;
}

// exported objects
const authenticationService = {
    login,
    logout,
    handleAuthentication,
    setSession,
    isAuthenticated,
    getAccessToken
};
export { authenticationService } 