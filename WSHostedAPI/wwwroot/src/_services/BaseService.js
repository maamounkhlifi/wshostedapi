import config from 'config';
import { authHeader, handleResponse } from '@/_helpers';

export default function BaseService() {
    this.controller = "";
}

BaseService.prototype = {};

BaseService.prototype.getAll = function () {
    const requestOptions = { method: 'GET', headers: authHeader() };
    return fetch(`${config.apiUrl}/${this.controller}`, requestOptions).then(handleResponse);
}

BaseService.prototype.getFilteredData  = function (filters) {
    const requestOptions = { method: 'PUT', headers: authHeader(), body: JSON.stringify(filters)};
    requestOptions.headers['content-type'] = 'application/json';
    return fetch(`${config.apiUrl}/${this.controller}`, requestOptions).then(handleResponse);
}

BaseService.prototype.getByID = function (id) {
    const requestOptions = { method: 'GET', headers: authHeader() };
    return fetch(`${config.apiUrl}/${this.controller}/${id}`, requestOptions).then(handleResponse);;
}

BaseService.prototype.update = function (entity, id) {
    const requestOptions = { method: 'PUT', headers: authHeader(), body: JSON.stringify(entity)};
    requestOptions.headers['content-type'] = 'application/json';
    return fetch(`${config.apiUrl}/${this.controller}/${id}`, requestOptions).then(handleResponse);;
}

BaseService.prototype.add = function (entity) {
    const requestOptions = { method: 'POST', headers: authHeader(), body: JSON.stringify(entity) };
    requestOptions.headers['content-type'] = 'application/json';
    return fetch(`${config.apiUrl}/${this.controller}`, requestOptions).then(handleResponse);;
}

BaseService.prototype.remove = function (id) {
    const requestOptions = { method: 'DELETE', headers: authHeader() };
    return fetch(`${config.apiUrl}/${this.controller}/${id}`, requestOptions).then(handleResponse);;
}

BaseService.prototype.getAllRecordsForField = function (query, field) {
    const requestOptions = { method: 'GET', headers: authHeader() };
    return fetch(`${config.apiUrl}/${this.controller}/GetAllRecordsForField/${query}/${field}`, requestOptions).then(handleResponse);
}