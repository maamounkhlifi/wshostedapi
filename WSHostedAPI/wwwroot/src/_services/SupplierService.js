import BaseService from './BaseService';
export { SupplierService };

function SupplierService() {
    this.controller = "Suppliers";
}

SupplierService.prototype = Object.create(BaseService.prototype); // Inherit!

