import BaseService from './BaseService';
export { ProductService };

function ProductService() {
    this.controller = "Products";
}

ProductService.prototype = Object.create(BaseService.prototype); // Inherit!

