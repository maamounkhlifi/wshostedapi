import BaseService from './BaseService';
export { CustomerService };

function CustomerService() {
    this.controller = "Customers";
}

CustomerService.prototype = Object.create(BaseService.prototype); // Inherit!

