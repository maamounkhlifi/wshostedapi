﻿using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Helpers.Extensions
{
    public static class RepositoryExtensions
    {        
        public static IQueryable<T> OrderBy<T>(this IQueryable<T> query, string sortField, int sortOrder)
        {
            if (!String.IsNullOrWhiteSpace(sortField))
            {
                var sortByPropName = sortField[0].ToString().ToUpper() + sortField.Substring(1);
                var param = Expression.Parameter(typeof(T), sortByPropName);
                var sortExpression = Expression.Lambda<Func<T, object>>
                (Expression.Convert(Expression.Property(param, sortByPropName), typeof(object)), param);
                switch (sortOrder)
                {
                    case 1:
                        return query.OrderBy<T, object>(sortExpression);
                    default:
                        return query.OrderByDescending<T, object>(sortExpression);
                }
            }
            else
                return query;
        }

        public static IQueryable<T> WhereDataHas<T>(this IQueryable<T> query, T filter)
        {

            Expression<Func<T, bool>> predicate = null;

            if (filter != null)
            {
                var predicateBuilder = PredicateBuilder.New<T>();
                PropertyInfo[] properties = typeof(T).GetProperties();
                foreach (PropertyInfo prop in properties)
                {
                    var value = filter.GetType().GetProperty(prop.Name).GetValue(filter, null);
                    if (value != null)
                    {
                        ParameterExpression parameter = Expression.Parameter(typeof(T));
                        Expression property = Expression.Property(parameter, prop.Name);
                        Expression target = Expression.Constant(value);
                        Expression<Func<T, bool>> lambda;
                        if (value.GetType().IsValueType || value is string)
                        {
                            if (value is string)
                            {
                                if (!string.IsNullOrWhiteSpace((string)value))
                                {
                                    var Method = typeof(string).GetMethod("StartsWith", new[] { typeof(string) });
                                    lambda = Expression.Lambda<Func<T, bool>>(Expression.Call(property, Method, target), parameter);
                                    predicate = predicateBuilder.And(lambda);
                                }
                            }
                            else if (value is DateTime?)
                            {
                                lambda = Expression.Lambda<Func<T, bool>>(Expression.LessThan(property, Expression.Convert(Expression.Constant(((DateTime?)value).Value.AddDays(1).Date), prop.PropertyType)), new[] { parameter });
                                predicate = predicateBuilder.And(lambda);
                                lambda = Expression.Lambda<Func<T, bool>>(Expression.GreaterThanOrEqual(property, Expression.Convert(Expression.Constant(((DateTime?)value).Value.Date), prop.PropertyType)), new[] { parameter });
                                predicate = predicateBuilder.And(lambda);
                            }
                            else
                            {
                                lambda = Expression.Lambda<Func<T, bool>>(Expression.Equal(property, Expression.Convert(Expression.Constant(value), prop.PropertyType)), new[] { parameter });
                                predicate = predicateBuilder.And(lambda);
                            }
                        }
                    }
                }
            }
            return predicate != null ? query.Where(predicate) : query;
        }
        public static List<string> GetAllRecordsForField<T>(this IQueryable<T> dataQuery, string query, string field)
        {
            Expression<Func<T, bool>> lambda = null;

            if (!string.IsNullOrWhiteSpace((string)query))
            {
                ParameterExpression parameter = Expression.Parameter(typeof(T));
                Expression property = Expression.Property(parameter, field);
                Expression target = Expression.Constant(query);            
                var Method = typeof(string).GetMethod("StartsWith", new[] { typeof(string) });
                lambda = Expression.Lambda<Func<T, bool>>(Expression.Call(property, Method, target), parameter);
            }
            return lambda != null ? dataQuery.Where(lambda).Select(x => x.GetType().GetProperty(field).GetValue(x).ToString()).ToList<string>() : new List<string>();
        }
    }
}
