﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts.Services
{
    public interface IUserService: IServiceBase<User>
    {
        User Authenticate(LoginModel user);       
    }
}