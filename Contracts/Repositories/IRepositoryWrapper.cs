﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public interface IRepositoryWrapper
    {
        ICustomerRepository Customer { get; }
        IOrderRepository Order { get; }
        IOrderItemRepository OrderItem { get; }
        IProductRepository Product { get; }
        ISupplierRepository Supplier { get; }
        IUserRepository User { get; }

        Task Save();
    }
}
