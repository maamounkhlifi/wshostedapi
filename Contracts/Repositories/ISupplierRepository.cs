﻿using Entities.Models;

namespace Contracts
{
    public interface ISupplierRepository : IRepositoryBase<Supplier>
    {
    }
}