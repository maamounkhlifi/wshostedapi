﻿using Entities.Models;

namespace Contracts
{
    public interface IOrderItemRepository : IRepositoryBase<OrderItem>
    {
    }
}